// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "gwemainwin.hpp"


GWEMainWin::GWEMainWin(QWidget *parent)
    : QMainWindow(parent),
    recentFiles(nullptr),
    howToOpenList(QMessageBox::Question, tr("How do you want to open this file list?"),
                  tr("Do you want to open the files saved in this list, or open this file as it is?"),
                  QMessageBox::NoButton, this),
    s_prevTab(QKeySequence("CTRL+PGUP"), this, SLOT(previousTab())),
    s_prevTab2(QKeySequence("CTRL+SHIFT+TAB"), this, SLOT(previousTab())),
    s_nextTab(QKeySequence("CTRL+PGDOWN"), this, SLOT(nextTab())),
    s_nextTab2(QKeySequence("CTRL+TAB"), this, SLOT(nextTab())),
    s_nbsp(QKeySequence("CTRL+SHIFT+SPACE"), this, [&](){ CURR_ED->insertHtml("&nbsp;"); }),
    s_nbh(QKeySequence("CTRL+SHIFT+-"), this, [&](){ CURR_ED->insertHtml("&#x2011;"); }),
    s_formatlessPasting(QKeySequence("CTRL+SHIFT+V"), this, SLOT(pasteWithoutFormatting())),
    prevEditor(nullptr), globalAutosaveTimer(this),
    tabSizeDialog(this)
{
    qApp->setWindowIcon(QIcon(DIR + "/resources/icon.png"));
    setCentralWidget(&tabs);

    initWidgets();

    if(params.fileExists())
    {
        if(params.load())
        {
            defaultFont.fromString(params.defaultFont);
            oldWidth = params.width; oldHeight = params.height;
            resize(oldWidth, oldHeight);
            if(params.maximized)
                setWindowState(Qt::WindowMaximized);
            globalAutosaveTimer.setInterval(params.autosaveDelay);
            SearchWindow::minimumLength = params.search_minLength;
        }
        else
            QMessageBox::critical(this, tr("Error"), tr("For an unknown reason, the parameters file (params.set) could not be loaded."));
    }
    createMenu();

    openFiles = howToOpenList.addButton(tr("Open files"), QMessageBox::AcceptRole);
    openFilesDoNotAskAgain = howToOpenList.addButton(tr("Open files and do not ask again"), QMessageBox::AcceptRole);
    openListAsIs = howToOpenList.addButton(tr("Open the list as is"), QMessageBox::RejectRole);
    howToOpenList.addButton(QMessageBox::Cancel);
    howToOpenList.setEscapeButton(QMessageBox::Cancel);

    QStringList args = QCoreApplication::arguments();
    if(args.count() > 1)
        open(args.mid(1));
    else
        newTab();

    tabs.setMovable(true);

    connect();

    QObject::connect(&tabs, SIGNAL(currentChanged(int)), this, SLOT(tabChanged()));
    QObject::connect(&tabs, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTab(int)));
    QObject::connect(&tabs, SIGNAL(closeAllExcept(int)), this, SLOT(closeAllExcept(int)));
    QObject::connect(&globalAutosaveTimer, SIGNAL(timeout()), this, SLOT(autosaveAll()));

    setWindowTitle(CURR_ED->getFileName() + " – " + GWE_VER); // GWE == Glân's Wonderful Editor ! Fufufu...oui mon ego va bien...
                                                             // GpaWa : Glan's powerful awesome wonderful astute
                                                            // and of course, "pawa" is also "power" misspelled.

    CURR_ED->setFocus();
}

GWEMainWin::~GWEMainWin()
{
    // dtor
    // a lot of stuff should be deleted here, be it won't be because this window closing means the whole program closing
    // so delete instructions would be a needless additional load
}


#define Connect(action) QObject::connect(a_##action, &QAction::triggered, CURR_ED, &Editor::action)
void GWEMainWin::connect()
{
    Connect(save);
    Connect(saveAs);
    Connect(rename);
    Connect(upper);
    Connect(lower);
    Connect(changeColor);
    Connect(changeBackgroundColor);
    Connect(showInExplorer);
    a_showInExplorer->setEnabled(CURR_ED->hasFile());
}

#define Disconnect(action) QObject::disconnect(a_##action, &QAction::triggered, prevEditor, &Editor::action)
void GWEMainWin::disconnect()
{
    if(!prevEditor)
        return;

    Disconnect(save);
    Disconnect(saveAs);
    Disconnect(rename);
    Disconnect(upper);
    Disconnect(lower);
    Disconnect(changeColor);
    Disconnect(changeBackgroundColor);
    Disconnect(showInExplorer);
}


bool GWEMainWin::fileDoesNotExist(const QString& path)
{
    bool created = false;

    if(QMessageBox::warning(this, tr("Nonexistent file"),
                            tr("The file\n%1\ndoes not exist anymore. Do you want to create it ?").arg(path),
                            QMessageBox::Yes | QMessageBox::No)
            == QMessageBox::Yes)
    {
        // Creating the file
        QFile tempFile(path);

        if(tempFile.open(QIODevice::WriteOnly))
        {
            tempFile.close();
            created = true;
        }
        else
            QMessageBox::critical(this, tr("Error"), tr("The file could not be created:\n") + path);
    }
    else
    {
        recentFiles->remove(path);
    }

    return created;
}



void GWEMainWin::newTab(const QString &path)
{
    QWidget *host = new QWidget;
    Editor *newEditor = new Editor(host, defaultFont, params.tabulationSize);

    bool canCreate = true, error = false;

    if(!path.isEmpty())
    {
        if(QFile::exists(path))
            error = !newEditor->open(path);
        else
        {
            if(fileDoesNotExist(path))
                error = !newEditor->open(path);
            else
                canCreate = false;
        }
    }

    if(error)
        canCreate = false;

    if(canCreate)
    {
        int index = tabs.count();

        if(index)
            prevEditor = CURR_ED;

        QVBoxLayout *layout = new QVBoxLayout();
        layout->addWidget(newEditor);
        host->setLayout(layout);
        tabs.addTab(host, newEditor->getFileName());
        tabs.setTabToolTip(index, ED(index)->getPath());

        newEditor->changeAutosaveDelay(params.autosaveDelay);
        newEditor->setIndex(index);
        QObject::connect(this, SIGNAL(updateAutosaveDelay(int)), newEditor, SLOT(changeAutosaveDelay(int)));
        QObject::connect(newEditor, SIGNAL(newName(const QString&, int, const QString&)), this, SLOT(changeTitle(const QString&, int, const QString&)));
        QObject::connect(newEditor, SIGNAL(cursorPositionChanged()), this, SLOT(adjustFont()));
        QObject::connect(newEditor, SIGNAL(filesDropped(const QStringList&)), this, SLOT(open(const QStringList&)));

        if(tabs.count() == 1) // if there was no tab before...
        {
            QObject::connect(a_fonts, SIGNAL(currentFontChanged(QFont)), this, SLOT(setFont(QFont)));
            enableToolbarWidgets(true);
        }

        if(a_switchToTabWhenNew->isChecked())
            tabs.setCurrentIndex(tabs.count()-1);
    }
    else
    {
        delete host;

        if(error)
            QMessageBox::critical(this, tr("Openning error"), tr("Could not open\n") + path);
    }
}

void GWEMainWin::open()
{
    open( QFileDialog::getOpenFileNames(this, tr("Open a file"), QString(),
        tr("All files (*.*);;Raw text (*.txt);;GWE rich text (*.gwe);;HTML rich text (*.htm; *.html);;All supported rich texts (*.gwe; *.htm; *.html);;File lists (*.gwefl)")) );
}

void GWEMainWin::open(const QStringList& files)
{
    bool openList;
    for(int i(0) ; i < files.size() ; i++)
    {
        openList = false;

        if(files[i].endsWith(".gwefl", Qt::CaseInsensitive))
        {
            if(a_askHowToOpenFileLists->isChecked())
            {
                if(howToOpenList.exec() == QMessageBox::Cancel)
                    return;
                else if(howToOpenList.buttonRole(howToOpenList.clickedButton()) == QMessageBox::AcceptRole)
                {
                    openList = true;
                    if(howToOpenList.clickedButton() == openFilesDoNotAskAgain)
                        a_askHowToOpenFileLists->setChecked(false);
                }
            }
            else
                openList = true;
        }

        if(openList)
        {
            QFile file(files[i]);
            if(file.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                QTextStream stream(&file);
                open(stream.readAll().split('\n', Qt::SkipEmptyParts));
            }
            else
                QMessageBox::critical(this, tr("Error"), tr("Could not open\n") + files[i]);
        }
        else
        {
            bool found = false;

            recentFiles->add(files[i]);

            if(a_openTabIfFileOpened->isChecked())
            {
                for(int j(0) ; j < tabs.count() && !found ; j++)
                {
                    Editor* ed(ED(j));

                    if(ed->hasFile() && ed->getPath() == files[i] && !ed->modified())
                    {
                        tabs.setCurrentIndex(j);
                        found = true;
                    }
                }
            }

            if(!found)
            {
                Editor* edit = CURR_ED;

                if(edit && !edit->hasFile() && !edit->modified())
                {
                    if(QFile::exists(files[i]) || fileDoesNotExist(files[i]))
                    {
                        if(edit->open(files[i]))
                            a_showInExplorer->setEnabled(true);
                        else
                            QMessageBox::critical(this, tr("Error"), tr("Could not open\n") + files[i]);
                    }
                }
                else
                    newTab(files[i]);
            }

            recentFiles->saveList();
        }
    } // for(int i(0) ; i < adresses.size() ; i++)
}

void GWEMainWin::openRecent(QAction *action)
{
    if(action == recentFiles->getOpenAll())
        open(recentFiles->getAllPaths());
    else if(action == recentFiles->getClear())
        recentFiles->clearList();
    else
        open( QStringList(recentFiles->path(recentFiles->indexOf(action))) );
}

void GWEMainWin::showRecentFilesPaths(bool full)
{
    QList<QAction*> recent(recentFilesMenu->actions());
    for(int i = RecentFilesList::OFFSET ; i < recent.count() ; i++)
        recent[i]->setText( full ? recent[i]->toolTip() : getFileName(recent[i]->toolTip()) );
}

void GWEMainWin::saveAll()
{
    for(int i = 0 ; i < tabs.count() ; i++)
    {
        if(!ED(i)->hasFile())
            tabs.setCurrentIndex(i);

        ED(i)->save();
    }
}

void GWEMainWin::saveList()
{
    QString fileName(QFileDialog::getSaveFileName(this, tr("Save a list"), QString(), tr("File list (*.gwefl)")));

    if(!fileName.isEmpty())
    {
        QFile file(fileName);

        if(file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        {
            QTextStream stream(&file);

            for(int i = 0 ; i < tabs.count() ; i++)
                if(ED(i)->hasFile())
                    stream<<ED(i)->getPath()<<'\n';
        }
        else
            QMessageBox::critical(this, tr("Saving error"), tr("Saving impossible at\n%1\nCheck that the device %2 has not been removed and that the file is not read-only.").arg(fileName).arg(fileName.left(2)));
    }
}


void GWEMainWin::closeTab()
{
    closeTab(tabs.currentIndex());
}

void GWEMainWin::closeTab(int index)
{
    int close = QMessageBox::Yes;
    Editor *const editClosing = ED(index);

    if(editClosing->modified() && a_tabCloseCheck->isChecked())
    {
        tabs.setCurrentIndex(index);
        prevEditor = editClosing;

        close = QMessageBox::warning(this, tr("Unsaved modifications"),
                                     tr("Some modifications have not been saved.\nDo you want to save the file ?"),
                                     QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

        if(close == QMessageBox::Yes)
            editClosing->save();
    }

    if(close != QMessageBox::Cancel)
    {
        QObject::disconnect(this, SIGNAL(updateAutosaveDelay(int)), editClosing, SLOT(changeAutosaveDelay(int)));
        QObject::disconnect(editClosing, SIGNAL(newName(const QString&, int, const QString&)), this, SLOT(changeTitle(const QString&, int, const QString&)));
        QObject::disconnect(editClosing, SIGNAL(cursorPositionChanged()), this, SLOT(adjustFont()));

        tabs.removeTab(index);
        delete editClosing->parent(); // Because removeTab does not delete the widget, not doing this causes memory leaks
    }

    if(tabs.count() == 0)
    {
        QObject::disconnect(a_fonts, SIGNAL(currentFontChanged(QFont)), this, SLOT(setFont(QFont)));
        a_fonts->setCurrentFont(defaultFont);
        a_size->setValue(defaultFont.pointSize());
        a_showInExplorer->setEnabled(false);
        enableToolbarWidgets(false);
        setWindowTitle(GWE_VER);
    }
    else
        tabChanged();
}

void GWEMainWin::closeAllExcept(int index)
{
    if(!tryClosingAll(index))
        return;

    bool closeCheck = a_tabCloseCheck->isChecked();
    a_tabCloseCheck->setChecked(false);
    auto tab = tabs.widget(index);
    auto label = tabs.tabText(index);
    tabs.clear();
    tabs.addTab(tab, label);
    a_tabCloseCheck->setChecked(closeCheck);
}


void GWEMainWin::tabChanged()
{
    if(tabs.count() > 1)
        disconnect();

    if(tabs.count() > 0)
        connect();

    prevEditor = CURR_ED;

    if(tabs.currentIndex() >= 0)
    {
        setWindowTitle(CURR_ED->getFileName() + " – " + GWE_VER);
        adjustFont();

        for(int i = 0 ; i < tabs.count() ; i++)
            ED(i)->setIndex(i);
    }
}

void GWEMainWin::previousTab()
{
    if(tabs.count())
    {
        if(tabs.currentIndex() == 0)
            tabs.setCurrentIndex(tabs.count() - 1);
        else
            tabs.setCurrentIndex(tabs.currentIndex() - 1);
    }
}

void GWEMainWin::nextTab()
{
    if(tabs.count())
    {
        if(tabs.currentIndex() == tabs.count() - 1)
            tabs.setCurrentIndex(0);
        else
            tabs.setCurrentIndex(tabs.currentIndex() + 1);
    }
}


void GWEMainWin::pasteWithoutFormatting()
{
    formatlessPastingBuffer.paste();
    CURR_ED->insertPlainText(formatlessPastingBuffer.toPlainText());
    CURR_ED->ensureCursorVisible();
    formatlessPastingBuffer.clear();
}


void GWEMainWin::changeTabulationSize()
{
    auto& tabulationSize = params.tabulationSize;
    tabSizeDialog.setIntValue(tabulationSize);

    if(tabSizeDialog.exec())
    {
        tabulationSize = tabSizeDialog.intValue();

        for(int i = 0 ; i < tabs.count() ; i++)
            ED(i)->setTabStopDistance(tabulationSize);
    }
}

void GWEMainWin::changeDefaultFont()
{
    bool ok = false;
    defaultFont = QFontDialog::getFont(&ok, defaultFont, this, tr("Choose a font"));
    params.defaultFont = defaultFont.toString();

    if(tabs.count() == 0)
    {
       a_fonts->setCurrentFont(defaultFont);
       a_size->setValue(defaultFont.pointSize());
    }
    else
        for(int i(0) ; i < tabs.count() ; i++)
            ED(i)->setFont(defaultFont);

    params.save();
}

void GWEMainWin::changeSelectedTextFont()
{
    bool ok = false;
    QFont font(QFontDialog::getFont(&ok, CURR_ED->currentFont(), this, tr("Choose a font")));
    if(ok)
        CURR_ED->setCurrentFont(font);
}

#define SWITCH(prop) void GWEMainWin::switch##prop(bool toggled) { \
    QFont font(CURR_ED->currentFont()); \
    font.set##prop(toggled); \
    CURR_ED->setCurrentFont(font); \
}
SWITCH(Bold)
SWITCH(Italic)
SWITCH(Underline)
SWITCH(StrikeOut)


void GWEMainWin::adjustFont()
{
    QFont font(CURR_ED->currentFont());
    a_bold->setChecked(font.bold());
    a_italic->setChecked(font.italic());
    a_underline->setChecked(font.underline());
    a_strike->setChecked(font.strikeOut());
    a_fonts->setCurrentFont(font);
    a_size->setValue(font.pointSize());
}

void GWEMainWin::setFont(const QFont& font)
{
    if(a_fonts->hasFocus())
    {
        CURR_ED->setFontFamily(font.family());
        CURR_ED->setFocus();
    }
}

void GWEMainWin::setTextSize(double size)
{
    if(a_size->hasFocus())
    {
        CURR_ED->setFontPointSize(size);
        CURR_ED->setFocus();
    }
}

void GWEMainWin::insertList(QTextListFormat::Style style)
{
    CURR_ED->textCursor().insertList(style);
}


void GWEMainWin::editAutosaveDelay()
{
    auto& autosaveDelay = params.autosaveDelay;
    bool changed = false;
    int timeInMinutes = QInputDialog::getInt(this, tr("Autosave"),
        tr("Set the delay between two autosaves in minutes.\n(0 = no autosave)"),
        autosaveDelay / 60000, 0, 30, 1, &changed);

    if(changed)
    {
        autosaveDelay = timeInMinutes * 60000;

        if(a_globalAutosave->isChecked())
        {
            globalAutosaveTimer.stop();

            if(autosaveDelay != 0)
            {
                globalAutosaveTimer.setInterval(autosaveDelay);
                globalAutosaveTimer.start();
            }
        }
        else
            emit updateAutosaveDelay(autosaveDelay);

        saveParams();
    }
}

void GWEMainWin::setAutosaveGlobal(bool global)
{
    if(global)
    {
        for(int i = 0 ; i < tabs.count() ; i++)
            ED(i)->changeAutosaveDelay(0);

        globalAutosaveTimer.start();
    }
    else
    {
        globalAutosaveTimer.stop();

        for(int i = 0 ; i < tabs.count() ; i++)
            ED(i)->changeAutosaveDelay(params.autosaveDelay);
    }
}

void GWEMainWin::autosaveAll()
{
    for(int i = 0 ; i < tabs.count() ; i++)
        ED(i)->autosave();
}


void GWEMainWin::changeTitle(const QString& name, int index, const QString& oldName)
{
    tabs.setTabText(index, name);
    tabs.setTabToolTip(index, ED(index)->getPath());
    setWindowTitle(CURR_ED->getFileName() + " – " + GWE_VER);

    if(!oldName.isEmpty())
    {
        recentFiles->remove(oldName);
        recentFiles->add(ED(index)->getPath());
    }
}


void GWEMainWin::resizeEvent(QResizeEvent *event)
{
    oldWidth = event->oldSize().width();
    oldHeight = event->oldSize().height();
    event->accept();
}


void GWEMainWin::closeEvent(QCloseEvent *event)
{
    bool acceptEvent = true;
    if(a_windowCloseCheck->isChecked() && tabs.count() != 0)
        acceptEvent = tryClosingAll();

    if(acceptEvent)
        acceptClose(event);
    else
        event->ignore();
}


bool GWEMainWin::tryClosingAll(int except)
{
    bool acceptClose = true;
    auto userChoice = QMessageBox::NoButton;
    std::vector<int> modifiedTabs;

    for(int i = 0 ; i < tabs.count() ; i++)
        if(i != except && ED(i)->modified())
            modifiedTabs.push_back(i);

    auto nModified = modifiedTabs.size();

    if(a_windowCloseCheck->isChecked()) for(std::size_t i = 0 ; i < nModified ; i++)
    {
        tabs.setCurrentIndex(modifiedTabs[i]);
        if(userChoice == QMessageBox::YesToAll)
        {
            if(!CURR_ED->save())
                acceptClose = false;
            continue;
        }

        const QString message(tr("The document \"%1\" have not been saved.\nDo you want to save it?").arg(CURR_ED->getFileName()));
        userChoice = QMessageBox::warning(this, tr("Unsaved document"), message,
            nModified == 1 ? QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel
            : QMessageBox::Yes | QMessageBox::YesToAll | QMessageBox::No | QMessageBox::NoToAll | QMessageBox::Cancel);

        switch(userChoice)
        {
        case QMessageBox::Yes:
        case QMessageBox::YesToAll:
            if(!CURR_ED->save())
                acceptClose = false;
            break;

        case QMessageBox::No:
            break;

        case QMessageBox::NoToAll:
            return true;

        default:
            return false;
        }
    }

    return acceptClose;
}

void GWEMainWin::acceptClose(QCloseEvent *event)
{
    saveParams();
    recentFiles->saveList();
    event->accept();
}
