#ifndef TABS_H
#define TABS_H

// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////



#include <QTabWidget>
#include <QTabBar>
#include <QMouseEvent>


class Tabs : public QTabWidget
{
    Q_OBJECT

    class TabBar : public QTabBar
    {
    protected:
        void mouseReleaseEvent(QMouseEvent *event);
    };


public:
    Tabs();
    virtual ~Tabs() {}

    void showMenuFor(int index);

signals:

    void closeAllExcept(int index);
};

#endif // TABS_H
