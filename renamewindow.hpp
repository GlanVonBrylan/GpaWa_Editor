#ifndef RENAMEWINDOW_HPP
#define RENAMEWINDOW_HPP

// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include <QDialog>
#include <QGridLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>


//const char* const SUPPORTED_EXTENSIONS[4] = { ".txt", ".gwe", ".htm", ".html" };
const QStringList SUPPORTED_EXTENSIONS = { ".txt", ".gwe", ".htm", ".html" };


class RenameWindow : public QDialog
{
    Q_OBJECT

public:
    // Expects the sole name of the file for the fileName parameter, not the whole absolute path.
    RenameWindow(QWidget *parent, const QString& fileName);

    QString getName();

private:
    QGridLayout layout;

    QLineEdit nameRoot;
    QComboBox extensions;
    QPushButton ok, cancel;
};

#endif // RENAMEWINDOW_HPP
