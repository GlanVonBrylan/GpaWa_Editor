#ifndef GWEMAINWIN_H
#define GWEMAINWIN_H

// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


// Qt headers //

#include <QDir>

#include <QMainWindow>
#include <QCloseEvent>

#include <QMenuBar>
#include <QToolBar>

#include <QFontComboBox>
#include <QDoubleSpinBox>
#include <QFontDialog>
#include <QInputDialog>

#include <QVBoxLayout>

#include <QShortcut>


// STD headers //

#include <map>


// GWE headers //

#include "params.hpp"
#include "tabs.hpp"
#include "toolbarcustomizer.hpp"
#include "recentfileslist.hpp"
#include "searchwindow.hpp"
#include "about.hpp"
#include "help.hpp"


#define CURR_ED (tabs.currentWidget() ? tabs.currentWidget()->findChild<Editor *>() : nullptr)
#define ED(i) tabs.widget(i)->findChild<Editor *>()


class ListAction : public QAction
{
    Q_OBJECT

public:
    const QTextListFormat::Style style;

    ListAction(QTextListFormat::Style s, const QString& icon, const QString& text, QObject* parent = nullptr)
        : QAction(QIcon(icon), text, parent), style(s)
    {
        QObject::connect(this, SIGNAL(triggered(bool)), this, SLOT(onTriggered()));
    }

signals:
    void triggered(QTextListFormat::Style);

public slots:
    void onTriggered()
    {
        emit triggered(style);
    }
};



class GWEMainWin : public QMainWindow
{
    Q_OBJECT

public:
    explicit GWEMainWin(QWidget *parent = nullptr);
    ~GWEMainWin();

    void updateToolbar();
    void enableToolbarWidgets(bool enable);

signals:
    void updateAutosaveDelay(int milliseconds);

public slots:
    // Set to dark theme if true, light theme otherwise.
    void setDarkTheme(bool dark);

    // Inlined function that sets the tabs closeable.
    // this has been made because QTabWidget::setTabsCLoseable(bool) is not a slot
    void setTabsClosable(bool closeable) { tabs.setTabsClosable(closeable); }

    // Show the toolbar editing window.
    void changeToolbar();

    // Methos called by the toolbar editing window whenever a case gets checked or unchecked.
    void toolbarChanged(std::uint8_t index);


    // Connects all the signals and slots related to the current Editor.
    void connect();

    // Disconnects all the signals and slots related to the current Editor.
    void disconnect();


    // Creates a new tab, making it load the file at 'path' if it is not empty.
    void newTab(const QString &path = QString());

    // Opens a window that asks the user to choose a file to open.
    // The opened file is either opened in the current tab if it is empty and has no associated file, either in a new tab.
    void open();

    // Opens the given files, either in the current tab if it is empty and has no associated file, either in a new tab.
    void open(const QStringList &files);

    // Called whenever an action from the "Open recent" menu is triggered.
    void openRecent(QAction *action);

    // Show the recent files' full paths if 'full' is true, otherwise only shows their file names.
    void showRecentFilesPaths(bool full);

    // Saves all the tabs, asking the user to choose a file path if necessary.
    void saveAll();

    // Saves all the file paths as a list
    void saveList();


    // Closes the tab at the current 'index'.
    void closeTab();
    // Closes the tab at the given 'index'.
    void closeTab(int index);
    // Closes all tabs except the one at the given 'index'.
    void closeAllExcept(int index);


    // Does whatever has to be done when the user changes tab (changing the window's title, (dis)connecting signals and slots, etc)
    void tabChanged();

    // Called when the user types CTRL+PGUP
    void previousTab();

    // Called when the user types CTRL+PGDOWN
    void nextTab();


    void pasteWithoutFormatting();


    // Asks the user to choose a new tabulation size, in pixels.
    void changeTabulationSize();

    // Asks the user to choose a new default font.
    void changeDefaultFont();

    // Asks the user to choose a new font for the selected text.
    void changeSelectedTextFont();


    // Switches the selected text boldness.
    void switchBold(bool grs);

    // Switches the selected text italicness (this word doesn't exists shut up).
    void switchItalic(bool itl);

    // Switches the selected text underliness (this doesn't exist either; will you shut your filthy mouth?!).
    void switchUnderline(bool slg);

    // Switches the selected text strikeness (I am sick of you reminding me which words exist and which doesn't).
    void switchStrikeOut(bool str);

    // Adjusts the state of the "font", "font size", "bold", "italic", "underlined" and "striked" buttons so they match the text's state at the cursor position.
    void adjustFont();

    // Changes the font of the selected text.
    void setFont(const QFont& font);

    // Changes the size of the selected text.
    void setTextSize(double size);

    // Inserts a list where the cursor is.
    void insertList(QTextListFormat::Style style);


    // Asks the user to choose a new autosave delay bewteen 0 and 30 minutes.
    // If he chooses 0 minutes, there will be no autosave delay.
    void editAutosaveDelay();

    // Sets the autosave mode to global if 'global' is true, otherwise to individual.
    void setAutosaveGlobal(bool global);

    // Autosaves every tab.
    void autosaveAll();


    // Saves the parameters in the params.set file.
    void saveParams();


    // Changes the window's title in order to make it match the current tab's.
    void changeTitle(const QString& name, int index, const QString& oldName);


    // Accepts the given QCloseEvent after saving the parameters.
    void acceptClose(QCloseEvent *event);


    // Overloaded method.
    // Handles the resise events.
    void resizeEvent(QResizeEvent *event);

    // Overloaded method.
    // Handles the closing events.
    void closeEvent(QCloseEvent *event);

    // Returns true if the user accepted closing everything, false otherwise
    bool tryClosingAll(int except = -1);

private:
    Params params;

    //Initializes the widgets that need to be
    void initWidgets();
    // Tests if the current parameters file is an old or a new one, then calls loadParams(QTextStream&,int) accordingly.
    void prepareParamsLoading();

    // Initializes the menus.
    void createMenu();

    // Method called when trying to open a file that doesn't exist.
    // Asks the user if he wants to create the file.
    // Returns true if he does. Otherwise removes the file path from the recent files list and returns false.
    bool fileDoesNotExist(const QString& path);

    QToolBar *toolbar;
    QFontComboBox *a_fonts;
    QDoubleSpinBox *a_size;

    // File menu actions
    QAction *a_new, *a_open, *a_save, *a_saveAs, *a_rename, *a_saveAll, *a_saveList, *a_showInExplorer, *a_closeTab;
    QMenu *recentFilesMenu;
    RecentFilesList *recentFiles;

    // Options menu actions
    QAction *a_switchToTabWhenNew, *a_switchToTabWhenOpen,
            *a_openTabIfFileOpened,
            *a_askHowToOpenFileLists, *a_saveAsRichTextByDefault,
            *a_toolbarShown, *a_customToolbar, *a_tabsClosingButton,
            *a_showFullRecentFilesPaths,
            *a_tabCloseCheck, *a_windowCloseCheck,
            *a_autosaveDelay, *a_globalAutosave,
            *a_darkTheme;

    // Lists-related actions
    ListAction *la_disc, *la_circle, *la_square, *la_decimal, *la_lowerAlpha, *la_upperAlpha, *la_lowerRoman, *la_upperRoman;

    // Other actions
    QAction *a_selectedTextFont, *a_changeColor, *a_changeBackgroundColor, *a_bold, *a_italic, *a_underline, *a_strike, *a_upper, *a_lower, *a_hr, *a_help;


    // QMessageBox showed to ask how to open a file list
    QMessageBox howToOpenList;
    QAbstractButton *openFiles, *openFilesDoNotAskAgain, *openListAsIs;


    // Shortcut for changing tab
    QShortcut s_prevTab, s_prevTab2, s_nextTab, s_nextTab2, s_nbsp, s_nbh, s_formatlessPasting;

    QTextEdit formatlessPastingBuffer;


    QAction *toolbarActions[NB_TOOLBAR_WIDGETS];

    QFont defaultFont;
    Editor *prevEditor;
    Tabs tabs;

    QTimer globalAutosaveTimer;

    QInputDialog tabSizeDialog;

    QString *customColors;

    int oldWidth, oldHeight;
};

#endif // GWEMAINWIN_H
