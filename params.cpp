// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////

#include "params.hpp"
#include <QColorDialog>


QTextStream& operator>>(QTextStream& stream, bool& b)
{
    int val;
    stream>>val;
    b = val;
    return stream;
}


Params::Params()
{
    displayTBActions.fill(true);
}


bool Params::load()
{
    enum category {
        DEFAULT_FONT,
        SIZE,
        OPTIONS,
        AUTOSAVE,
        TOOLBAR,
        WINDOW_MAXIMIZED,
        SEARCH,
        COLORS,
        THEME,
    };

    std::map<QString, category> parameters;
    parameters["#defaultFont"] = DEFAULT_FONT;
    parameters["#size"] = SIZE;
    parameters["#options"] = OPTIONS;
    parameters["#autosave"] = AUTOSAVE;
    parameters["#toolbar"] = TOOLBAR;
    parameters["#windowMaximized"] = WINDOW_MAXIMIZED;
    parameters["#search"] = SEARCH;
    parameters["#colors"] = COLORS;
    parameters["#theme"] = THEME;

    int paramValue;

    QFile file(FILE_PATH);

    if(!file.open(QIODevice::ReadOnly))
        return false;

    QTextStream stream(&file);
    QString currentParam;
    uint nbLines,
         nbColors;
    float fullVer;

    stream>>currentParam>>nbLines>>fullVer;
    const int version = fullVer;

    while(!stream.atEnd())
    {
        stream>>currentParam>>nbLines;

        switch(parameters[currentParam])
        {
        case DEFAULT_FONT:
            stream.skipWhiteSpace();
            defaultFont = stream.readLine();
            break;

        case SIZE:
            stream>>width>>height;
            break;

        case OPTIONS:
            stream>>switchToTabWhenNew>>switchToTabWhenOpen;
            stream>>tabCloseCheck>>windowCloseCheck;
            stream>>tabsClosingButton;
            stream>>openTabIfFileOpened;
            stream>>globalAutosave;
            stream>>showFullRecentFilesPaths;

            if(nbLines > 8)
            {
                stream>>tabulationSize;

                if(nbLines > 9)
                {
                    stream>>askHowToOpenFileLists;
                    stream>>saveAsRichTextByDefault;

                    /*if(nbLines > 10)
                    {
                        // for future new parameters
                    }*/
                }
            }
            break;

        case AUTOSAVE:
            stream>>autosaveDelay;
            break;

        case TOOLBAR:
            stream>>paramValue;
            switch(paramValue)
            {
            case 4:
                toolbarPos = Qt::TopToolBarArea;
                break;
            case 2:
                toolbarPos = Qt::RightToolBarArea;
                break;
            case 1:
                toolbarPos = Qt::LeftToolBarArea;
                break;
            case 8:
                toolbarPos = Qt::BottomToolBarArea;
                break;
            default:
                toolbarPos = Qt::NoToolBarArea;
            }

            for(uint i = 0 ; i < qMin(NB_TOOLBAR_WIDGETS, nbLines - 1) ; i++)
            {
                stream>>paramValue;
                displayTBActions[i] = paramValue;
            }

            if(version == 7)
            {
                for(uint i = NB_TOOLBAR_WIDGETS ; i > 9 ; i--) // Because the background color option has been added in position 9
                    displayTBActions[i] = displayTBActions[i-1];
                displayTBActions[9] = true;
            }
            break;

        case WINDOW_MAXIMIZED:
            stream>>maximized;
            break;

        case SEARCH:
            stream>>search_minLength;
            break;

        case COLORS:
            uint i;
            stream.skipWhiteSpace();
            nbColors = qMin(nbLines, (unsigned)QColorDialog::customCount());
            for(i = 0 ; i < nbColors ; i++)
                QColorDialog::setCustomColor(i, stream.readLine());

            for( ; i < nbLines ; i++)
                stream.readLine();
            break;

        case THEME:
            stream>>darkTheme;
            break;

        default: // unknown info
            QString error("params.set unknown param : " + currentParam + " (" + QString::number(nbLines) + " lines)");
            for(uint i = 0 ; i < nbLines ; i++)
                error.append(stream.readLine());

            GWEGlobal::logError(error);
            break;
        } // switch(parameters[currentParam])
    } // while(!stream.atEnd())

    return true;
}


bool Params::save()
{
    /*
     * Method :
     * #paramName linesCount
     * line1
     * line2
     * etc
     *
     * Example :
     * #version 1
     * 2
     * #size 2
     * 800
     * 600
     * #options 4
     * 0
     * 0
     * 1
     * 0
     *
     * Important: no empty lines !
     * Also important: the first one should always be #version, with 1 line !
     */

    QFile file(FILE_PATH);
    qDebug()<<FILE_PATH;
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        return false;

    QTextStream stream(&file);
    uint i;

    stream<<"#version 1\n"<<GWE_VERNUM
          <<"\n#defaultFont 1\n"<<defaultFont
          <<"\n#size 2\n"<<width<<'\n'<<height
          <<"\n#windowMaximized 1\n"<<maximized

          <<"\n#options 11\n"
            <<switchToTabWhenNew<<'\n'<<switchToTabWhenOpen
            <<'\n'<<tabCloseCheck<<'\n'<<windowCloseCheck<<'\n'<<tabsClosingButton
            <<'\n'<<openTabIfFileOpened<<'\n'<<globalAutosave<<'\n'<<showFullRecentFilesPaths
            <<'\n'<<tabulationSize
            <<'\n'<<askHowToOpenFileLists
            <<'\n'<<saveAsRichTextByDefault

          <<"\n#autosave 1\n"<<autosaveDelay
          <<"\n#search 1\n"<<search_minLength;

    const uint COLOR_COUNT = QColorDialog::customCount();
    stream<<"\n#colors "<<COLOR_COUNT;
    for(i = 0 ; i < COLOR_COUNT ; i++)
        stream<<'\n'<<QColorDialog::customColor(i).name();


    stream<<"\n#toolbar "<<(NB_TOOLBAR_WIDGETS + 1)<<'\n'<<toolbarPos;
    for(i = 0 ; i < NB_TOOLBAR_WIDGETS ; i++)
        stream<<'\n'<<displayTBActions[i];

    stream<<"\n#theme 1\n"<<darkTheme;

    file.close();
    return true;
}

