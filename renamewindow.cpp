// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "renamewindow.hpp"


RenameWindow::RenameWindow(QWidget *parent, const QString& fileName)
    : QDialog(parent, Qt::Dialog),
    layout(),
    nameRoot(),
    extensions(),
    ok(tr("Rename")), cancel(tr("Cancel"))
{
    setWindowTitle(tr("Rename file"));
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    int extPos = fileName.lastIndexOf('.');
    if(extPos == -1)
        extPos = fileName.size() + 1;

    nameRoot.setText(fileName.left(fileName.size() - extPos));
    QString curExt(fileName.right(extPos).toLower());

    int extIndex = SUPPORTED_EXTENSIONS.indexOf(curExt);

    if(extIndex == -1)
        extensions.addItem(curExt);
    else
        extensions.addItem(SUPPORTED_EXTENSIONS[extIndex]);

    for(int i = 0 ; i < SUPPORTED_EXTENSIONS.size() ; i++)
        if(i != extIndex)
            extensions.addItem(SUPPORTED_EXTENSIONS[i]);

    QObject::connect(&ok, SIGNAL(clicked(bool)), this, SLOT(accept()));
    QObject::connect(&cancel, SIGNAL(clicked(bool)), this, SLOT(reject()));

    nameRoot.setMinimumWidth(300);
    layout.addWidget(&nameRoot, 0, 0, 1, 5);
    layout.addWidget(&extensions, 0, 5);
    layout.addWidget(&ok, 1, 4);
    layout.addWidget(&cancel, 1, 5);
    setLayout(&layout);
}


QString RenameWindow::getName()
{
    return nameRoot.text() + extensions.currentText();
}
