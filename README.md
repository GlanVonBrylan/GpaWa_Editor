![GpaWa Editor logo](http://i39.servimg.com/u/f39/17/10/54/90/logo_g10.png)

Current version : 9.4

# Presentation
**GpaWa Editor** (GWE) is a small text editor, designed to allow simple rich text editing. It features bold, italic, underlines and striked text, change of colour, font family and font size, ordered and unordered lists, and horizontal lines (`<hr />`).<br />
It also has editing features like search & replace or switching the selected text to upper/lower case.<br />
The most original feature for this kind of software is the multi-tab editing, allowing you to work on several files in the same window. Note about this that GWE is *not* suited for code editing, is it does not not have any kind of syntax highlighting or auto-completion. It is meant for normal text editing.<br />
Also, if you need a more full-fledged document editor with spell checking, page formatting and such, you will have to opt for a bigger software like LibreOffice. But if you simply need to have a bit more than raw text, then GWE is for you. I personnally use it in addition to LibreOffice.

### Help Wanted
Hey, you are good at making icons? If you could re-make them so they're not that ugly anymore, I would really appreciate it. I'd like 64x64 at least (current is 24x24). You can find the resources in the download links below. <br />
I'm also looking for someone to re-make the logo, preferably in SVG format.<br />
You can contact me at [julesr1230@gmail.com](mailto:julesr1230@gmail.com), or on discord: `morganvonbrylan`.

### Download pre-built executables
Include resources. If you only want the resources, download the GNU/Linux 64-bits version, as it is the most lightweight.<br />
Windows : https://www.mediafire.com/file/xqsx9a97wksc1j5/GpaWa_Editor_setup.exe (8.07 Mio)<br />
GNU/Linux 64-bit : https://www.mediafire.com/file/hfjcgc8n14z7zgh/GpaWa_Editor_9.0_-_Linux_64-bit.tar.xz (695 kio)<br/>
GNU/Linux 32-bit : https://www.mediafire.com/file/t43rqlu7kw6an83/GpaWa_Editor_9.0_-_Linux_32-bit.tar.xz (696 kio)

**GNU/Linux users :** Here are the packets you will have to install:
- libqt5widgets5
- libqt5gui5
- libqt5core5
- libstdc++6
- libgcc_s1
- libc6

# Licence
**GpaWa Editor** is provided under the GNU Lesser General Public Licence version 3 (LGPL v3). For more details, see the COPYING.txt and COPYING_LESSER.txt files, or this link : [http://www.gnu.org/licenses/lgpl-3.0.en.html](http://www.gnu.org/licenses/lgpl-3.0.en.html) ![LGPL logo](http://www.gnu.org/graphics/lgplv3-147x51.png)

# Compiling
**GpaWa Editor** uses the [Qt framework](https://www.qt.io/). The easier way to compile it is therefore to install Qt Creator and to import the project in it.<br />
If you absolutely don't want to install Qt Creator... err... you should probably turn to non-Qt projects. Though I suppose compiling the source code of the framework, then using it as a normal library, should work. [Here it is. (384 Mio)](http://www.mediafire.com/file/6jadcgayw3lyy9p/qt-everywhere-src-5.10.0.7z) Note that I have not tried to compile this code, but this is the official source code given by The Qt Company, so if it does not work, it's not my fault.
