#ifndef RECENTFILESLIST_H
#define RECENTFILESLIST_H

// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include <QAction>
#include <QMenu>
#include <QStringList>
#include <QDate>

#include "editor.hpp"


const QString DATE_FORMAT("dd/MM/yyyy");


/**
 * \class RecentFilesList
 * \brief The class used to handle the recent files list.
 */
class RecentFilesList
{
    /*
     * This class uses the QAction's tooltip to hold the files' full paths.
     * This is obviously not the purpose of tooltips, but it makes things much easier.
     */
public:
    // This offset corresponds to the actions in the menu that are not file actions (here Open all, Clear, and a separator)
    static constexpr int OFFSET = 3;
    const QString FILE_PATH = DIR + "/recent.set";


    RecentFilesList();

    RecentFilesList(int maxPaths, QMenu *actionsMenu, QWidget *master);

    int count() { return pathsCount; }

    int getMax() { return max; }
    void setMax(int maximum);

    QAction* getOpenAll() { return a_openAll; }
    QAction* getClear() { return a_clear; }

    const QAction* action(int pos) { return menu->actions()[pos+OFFSET]; }
    QString path(int pos) { return menu->actions()[pos+OFFSET]->toolTip(); }
    QStringList getAllPaths();

    int indexOf(QAction *action) { return menu->actions().indexOf(action) - OFFSET; }
    int indexOf(const QString& path)
    {
        int pos = -1;
        QList<QAction*> actions = menu->actions();

        for(int i = OFFSET ; i - OFFSET < pathsCount && i < actions.size() ; i++)
            if(actions[i]->toolTip() == path)
                pos = i-OFFSET;

        return pos;
    }

    // Adds the given path to the recent files list. If it was already here, it is moved to the top.
    void add(QString path);

    bool remove(int pos);
    bool remove(const QString& path);

    void moveToStart(int pos);
    void moveToStart(const QString& path);

    void updateMenu();

    void clearList();

    bool saveList();

    void operator+=(const QString& path) { add(path); }
    QString operator[](int pos) { return menu->actions()[pos+2]->toolTip(); }

private:
    int max, pathsCount;
    QMenu *const menu;
    QAction *const a_openAll, *const a_clear;
    QWidget *const master;
    QStringList dates;
};

#endif // RECENTFILESLIST_H
