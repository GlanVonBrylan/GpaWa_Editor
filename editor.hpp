#ifndef EDITOR_H
#define EDITOR_H

// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include <QColorDialog>
#include <QFileDialog>
#include <QTextStream>
#include <QTextEdit>
#include <QTimer>
#include <QProgressBar>
#include <QScrollArea>
#include <QMessageBox>
#include <QAction>

#include <QDropEvent>
#include <QMimeData>

#include "global.hpp"
#include "renamewindow.hpp"



// Returns true if the given file name ends by .htm, .html or .gwe
bool isRichText(const QString& filename);

// Returns fileName without the path.
const QString getFileName(const QString &fileName);
// Returns the file's file name without the path, or "New document" if file is null.
const QString getFileName(const QFile *file);



/**
 * \class Editor
 * \brief The class that holds the text editor, also holding all the saving, loading, and other editing functions.
 *
 * Editor inherits from QTextEdit.
 * In addition of allowing the user to edit text, it holds all the other functions on the text to allow saving, saving as, change the color or swithc between upper and lower case.
 * It is also the one emitting the signal allowing the tabs to change title.
 * Every tab holds a QWidget that holds an Editor.
 */
class Editor : public QTextEdit
{
    Q_OBJECT

public:
    static QAction *saveAsRichTextByDefault;
    static const QString defaultStyleSheet;


    Editor(QWidget *parent, const QFont &font, int tabSize);
    ~Editor();

    bool modified() const { return changed; }

    int getIndex() const { return index; }
    void setIndex(int i) { index = i; }

    bool hasFile()        const { return file; }
    QString getPath()     const { return file ? file->fileName() : tr("New document"); }
    QString getFileName() const { return ::getFileName(file); }

signals:
    // Emitted the title of the tab must be changed, because the document was edited, for instance. If oldName is not empty, it means the file has been renamed.
    void newName(const QString& name, int index, const QString& oldName);

    // Emitted when files have been dropped to be opened
    void filesDropped(const QStringList& urls);

public slots:
    // Opens the file's folder in the file explorer.
    // Returns false if there is no file or opening the explorer failed; true otherwise.
    bool showInExplorer();

    // Loads the file at the given address
    // Returns true if the load succeeded, or false if it fails (will also show an error dialog)
    bool open(const QString& path);

    // Saves the holded text into the 'fichier' file. If 'fichier' is null, calls enregistrerSous instead.
    // Returns true if the saving was successful, false otherwise.
    bool save();

    // Asks the user to choose a new file path, creates a new QFile from it and stores it into 'fichier', then calls enregistrer().
    // Returns whatever enregistrer() returns, or false if the user gave an empty string instead of the file path.
    bool saveAs();

    // Allows to change the name of the file, without having to go through the 'Save as' function then deleting the old file.
    bool rename();

    // Puts the file's full path in the clipboard.
    void copyFullPath();

    // Changes 'changed' so it is true, then emits newName(String).
    void fileChanged();

    // Checks whether the last saved undo step was just overwritten
    void undoCommandAdded();

    // Changes the color of the selected text into a color picked by the user.
    void changeColor();

    // Changes the background color of the selected text into a color picked by the user.
    void changeBackgroundColor();

    // Switches the selected text into upper case.
    void upper();

    // Switches the selected text into lower case.
    void lower();

    // Changes the auto-save delay.
    // If the 'milliseconds' parameter is equal or less than 0, the autosave is turned off.
    void changeAutosaveDelay(int milliseconds);

    // Auto-saves.
    void autosave();



    // Overloaded method.
    // Manually handles undo/redo's
    void keyPressEvent(QKeyEvent *event);

    // Overloaded method.
    // Handles drops (from drags & drops)
    void dropEvent(QDropEvent *event);

private:
    QFile *file;
    bool changed;
    int lastSavedUndoSteps;
    bool lastPressIsUndoRedo;

    int index;

    QTimer autosaveTimer;
};

#endif // EDITOR_H
