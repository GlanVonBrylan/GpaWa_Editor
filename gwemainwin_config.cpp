// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "gwemainwin.hpp"


void GWEMainWin::initWidgets()
{
    tabSizeDialog.setWindowFlags(tabSizeDialog.windowFlags() & ~Qt::WindowContextHelpButtonHint);
    tabSizeDialog.setWindowTitle(tr("Change tabulation size"));
    tabSizeDialog.setLabelText(tr("Enter the tabulation size, in pixels:"));
    tabSizeDialog.setInputMode(QInputDialog::IntInput);
    tabSizeDialog.setIntRange(1, 500);

    for(uint i = 0 ; i < NB_TOOLBAR_WIDGETS ; i++)
        params.displayTBActions[i] = true;
}


#define OPTION(action, desc) \
    a_##action = new QAction(desc, this);\
    a_##action->setCheckable(true);\
    a_##action->setChecked(params.action);\
    menuOptions->addAction(a_##action);\
    QObject::connect(a_##action, &QAction::toggled, [&](bool b){params.action = b; params.save();});


void GWEMainWin::createMenu()
{
    toolbar = new QToolBar;
    toolbar->setFloatable(false);
    if(params.toolbarPos)
        addToolBar(params.toolbarPos, toolbar);

    QString dir(DIR + "/resources/file/");

    // File
    {
        QMenu *menuFile = menuBar()->addMenu(tr("&File"));

        a_new = new QAction(QIcon(dir + "new.png"), tr("New"), this);
        a_new->setToolTip(tr("New (Ctrl+N)"));
        menuFile->addAction(a_new);
        a_new->setShortcut(QKeySequence("CTRL+N"));
        QObject::connect(a_new, SIGNAL(triggered()), this, SLOT(newTab()));

        a_open = new QAction(QIcon(dir + "open.png"), tr("Open"), this);
        a_open->setToolTip(tr("Open (Ctrl+O)"));
        menuFile->addAction(a_open);
        a_open->setShortcut(QKeySequence("CTRL+O"));
        QObject::connect(a_open, SIGNAL(triggered()), this, SLOT(open()));

        recentFilesMenu = menuFile->addMenu(tr("Open &recent"));
        recentFilesMenu->addAction(tr("Open all"))
            ->setShortcut(QKeySequence("CTRL+SHIFT+O"));
        recentFilesMenu->addAction(tr("Clear list"));
        recentFilesMenu->addSeparator();
        recentFiles = new RecentFilesList(10, recentFilesMenu, this);
        QObject::connect(recentFilesMenu, SIGNAL(triggered(QAction*)), this, SLOT(openRecent(QAction*)));

        a_save = new QAction(QIcon(dir + "save.png"), tr("Save"), this);
        a_save->setToolTip(tr("Save (Ctrl+S)"));
        menuFile->addAction(a_save);
        a_save->setShortcut(QKeySequence("CTRL+S"));

        a_saveAs = new QAction(QIcon(dir + "saveAs.png"), tr("Save as..."), this);
        a_saveAs->setToolTip(tr("Save as... (Ctrl+Shift+S)"));
        menuFile->addAction(a_saveAs);
        a_saveAs->setShortcut(QKeySequence("CTRL+SHIFT+S"));

        a_rename = new QAction(QIcon(dir + "rename.png"), tr("Rename file"), this);
        a_rename->setToolTip(tr("Rename file (Ctrl+R)"));
        menuFile->addAction(a_rename);
        a_rename->setShortcut(QKeySequence("CTRL+R"));

        a_saveAll = new QAction(QIcon(dir + "saveAll.png"), tr("Save all"), this);
        a_saveAll->setToolTip(tr("Save all (Ctrl+Alt+S)"));
        menuFile->addAction(a_saveAll);
        a_saveAll->setShortcut(QKeySequence("CTRL+ALT+S"));
        QObject::connect(a_saveAll, SIGNAL(triggered()), this, SLOT(saveAll()));

        a_saveList = new QAction(QIcon(dir + "saveList.png"), tr("Save as a list"), this);
        a_saveList->setToolTip(tr("Save as a list (Ctrl+Shift+Alt+S)"));
        menuFile->addAction(a_saveList);
        a_saveList->setShortcut(QKeySequence("CTRL+SHIFT+ALT+S"));
        QObject::connect(a_saveList, SIGNAL(triggered()), this, SLOT(saveList()));

        menuFile->addSeparator();

        a_showInExplorer = new QAction(tr("Show file in explorer"), this);
        menuFile->addAction(a_showInExplorer);
        a_showInExplorer->setShortcut(QKeySequence("CTRL+ALT+O"));

        menuFile->addSeparator();

        a_closeTab = new QAction(QIcon(DIR + "/resources/tools/closeTab.png"), tr("Close the tab"), this);
        a_closeTab->setToolTip(tr("Close tab (Ctrl+W)"));
        menuFile->addAction(a_closeTab);
        a_closeTab->setShortcut(QKeySequence("CTRL+W"));
        QObject::connect(a_closeTab, SIGNAL(triggered()), this, SLOT(closeTab()));

        QAction *quitter = new QAction(tr("Quit"), this);
        menuFile->addAction(quitter);
        quitter->setShortcut(QKeySequence("CTRL+Q"));
        QObject::connect(quitter, SIGNAL(triggered()), this, SLOT(close()));
    }

    // Tools
    {
        QMenu *menuTools = menuBar()->addMenu(tr("T&ools"));
        dir = DIR + "/resources/tools/";

        QAction *a_tabulationSize = new QAction(tr("Change tabulation size"), this);
        menuTools->addAction(a_tabulationSize);
        QObject::connect(a_tabulationSize, SIGNAL(triggered()), this, SLOT(changeTabulationSize()));

        QAction *a_defaultFont = new QAction(tr("Change the default font"), this);
        menuTools->addAction(a_defaultFont);
        QObject::connect(a_defaultFont, SIGNAL(triggered()), this, SLOT(changeDefaultFont()));

        a_selectedTextFont = new QAction(QIcon(dir + "text/font.png"), tr("Selected text's font"), this);
        menuTools->addAction(a_selectedTextFont);
        QObject::connect(a_selectedTextFont, SIGNAL(triggered()), this, SLOT(changeSelectedTextFont()));

        a_fonts = new QFontComboBox();
        a_fonts->setToolTip(tr("Font"));
        a_fonts->setCurrentFont(defaultFont);
        QObject::connect(a_fonts, SIGNAL(currentFontChanged(QFont)), this, SLOT(setFont(QFont)));

        a_size = new QDoubleSpinBox();
        a_size->setToolTip(tr("Font size"));
        a_size->setValue(defaultFont.pointSize());
        a_size->setDecimals(0);
        a_size->setMaximum(2000);
        QObject::connect(a_size, SIGNAL(valueChanged(double)), this, SLOT(setTextSize(double)));

        a_changeColor = new QAction(QIcon(dir + "text/color.png"), tr("Selected text's color"), this);
        a_changeColor->setToolTip(tr("Color"));
        menuTools->addAction(a_changeColor);

        a_changeBackgroundColor = new QAction(QIcon(dir + "text/color_background.png"), tr("Selected text's background color"), this);
        a_changeBackgroundColor->setToolTip(tr("Background color"));
        menuTools->addAction(a_changeBackgroundColor);


        a_bold = new QAction(QIcon(dir + "text/bold.png"), tr("Bold"), this);
        a_bold->setToolTip(tr("Bold (Ctrl+B)"));
        a_bold->setShortcut(QKeySequence("CTRL+B"));
        a_bold->setCheckable(true);
        QObject::connect(a_bold, SIGNAL(triggered(bool)), this, SLOT(switchBold(bool)));

        a_italic = new QAction(QIcon(dir + "text/italic.png"), tr("Italic"), this);
        a_italic->setToolTip(tr("Italic (Ctrl+I)"));
        a_italic->setShortcut(QKeySequence("CTRL+I"));
        a_italic->setCheckable(true);
        QObject::connect(a_italic, SIGNAL(triggered(bool)), this, SLOT(switchItalic(bool)));

        a_underline = new QAction(QIcon(dir + "text/underline.png"), tr("Underline"), this);
        a_underline->setToolTip(tr("Underline (Ctrl+U)"));
        a_underline->setShortcut(QKeySequence("CTRL+U"));
        a_underline->setCheckable(true);
        QObject::connect(a_underline, SIGNAL(triggered(bool)), this, SLOT(switchUnderline(bool)));

        a_strike = new QAction(QIcon(dir + "text/strike.png"), tr("Strike"), this);
        a_strike->setToolTip(tr("Strike"));
        a_strike->setCheckable(true);
        QObject::connect(a_strike, SIGNAL(triggered(bool)), this, SLOT(switchStrikeOut(bool)));

        a_upper = new QAction(QIcon(dir + "text/case_upper.png"), tr("Switch the selected text to upper font"), this);
        menuTools->addAction(a_upper);

        a_lower = new QAction(QIcon(dir + "text/case_lower.png"), tr("Switch the selected text to lower font"), this);
        menuTools->addAction(a_lower);

        a_hr = new QAction(QIcon(dir + "hr.png"), tr("Insert an horizontal bar"), this);
        a_hr->setToolTip(tr("Horizontal bar (Ctrl+H)"));
        a_hr->setShortcut(QKeySequence("CTRL+H"));
        menuTools->addAction(a_hr);
        QObject::connect(a_hr, &QAction::triggered, [&](){ CURR_ED->insertHtml("<hr /><br />"); });

        // Lists
        {
            dir += "lists/";
            QMenu *menuLists = menuTools->addMenu(QIcon(dir + "discs.png"), tr("Insert a &list"));

            la_disc = new ListAction(QTextListFormat::Style::ListDisc, dir + "discs.png", tr("Points"), this);
            menuLists->addAction(la_disc);
            QObject::connect(la_disc, SIGNAL(triggered(QTextListFormat::Style)), this, SLOT(insertList(QTextListFormat::Style)));

            la_circle = new ListAction(QTextListFormat::Style::ListCircle, dir + "circles.png", tr("Circles"), this);
            menuLists->addAction(la_circle);
            QObject::connect(la_circle, SIGNAL(triggered(QTextListFormat::Style)), this, SLOT(insertList(QTextListFormat::Style)));

            la_square = new ListAction(QTextListFormat::Style::ListSquare, dir + "squares.png", tr("Squares"), this);
            menuLists->addAction(la_square);
            QObject::connect(la_square, SIGNAL(triggered(QTextListFormat::Style)), this, SLOT(insertList(QTextListFormat::Style)));

            la_decimal = new ListAction(QTextListFormat::Style::ListDecimal, dir + "decimal.png", tr("Decimal"), this);
            menuLists->addAction(la_decimal);
            QObject::connect(la_decimal, SIGNAL(triggered(QTextListFormat::Style)), this, SLOT(insertList(QTextListFormat::Style)));

            la_lowerAlpha = new ListAction(QTextListFormat::Style::ListLowerAlpha, dir + "lowerAlpha.png", tr("Lowercase letters"), this);
            menuLists->addAction(la_lowerAlpha);
            QObject::connect(la_lowerAlpha, SIGNAL(triggered(QTextListFormat::Style)), this, SLOT(insertList(QTextListFormat::Style)));

            la_upperAlpha = new ListAction(QTextListFormat::Style::ListUpperAlpha, dir + "upperAlpha.png", tr("Uppercase letters"), this);
            menuLists->addAction(la_upperAlpha);
            QObject::connect(la_upperAlpha, SIGNAL(triggered(QTextListFormat::Style)), this, SLOT(insertList(QTextListFormat::Style)));

            la_lowerRoman = new ListAction(QTextListFormat::Style::ListLowerRoman, dir + "lowerRoman.png", tr("Lowercase roman numbers"), this);
            menuLists->addAction(la_lowerRoman);
            QObject::connect(la_lowerRoman, SIGNAL(triggered(QTextListFormat::Style)), this, SLOT(insertList(QTextListFormat::Style)));

            la_upperRoman = new ListAction(QTextListFormat::Style::ListUpperRoman, dir + "upperRoman.png", tr("Uppercase roman numbers"), this);
            menuLists->addAction(la_upperRoman);
            QObject::connect(la_upperRoman, SIGNAL(triggered(QTextListFormat::Style)), this, SLOT(insertList(QTextListFormat::Style)));
        }

        menuTools->addSeparator();

        QAction *a_search = new QAction(tr("Search && replace"), this);
        a_search->setShortcut(QKeySequence("CTRL+F"));
        menuTools->addAction(a_search);
        QObject::connect(a_search, &QAction::triggered, [&]() {
            SearchWindow src(CURR_ED, this);
            return src.exec();
        });
    }

    // Options
    {
        QMenu *menuOptions = menuBar()->addMenu("O&ptions");

        OPTION(switchToTabWhenNew, tr("Immediately switch to the tab with \"New\""));

        OPTION(switchToTabWhenOpen, tr("Immediately switch to the tab with \"Open\""));

        OPTION(openTabIfFileOpened, tr("When opening an already openned file, switch to its tab"));

        OPTION(askHowToOpenFileLists, tr("Always ask how to open file lists"));

        OPTION(saveAsRichTextByDefault, tr("Save documents as rich text by default"));
        Editor::saveAsRichTextByDefault = a_saveAsRichTextByDefault;

        menuOptions->addSeparator();

        OPTION(showFullRecentFilesPaths, tr("Show full recent files paths"));
        QObject::connect(a_showFullRecentFilesPaths, SIGNAL(toggled(bool)), this, SLOT(showRecentFilesPaths(bool)));

        menuOptions->addSeparator();

        a_toolbarShown = new QAction(tr("Show the toolbar"), this);
        a_toolbarShown->setCheckable(true);
        a_toolbarShown->setChecked(params.toolbarPos);
        QObject::connect(a_toolbarShown, SIGNAL(toggled(bool)), toolbar, SLOT(setVisible(bool)));
        QObject::connect(a_toolbarShown, &QAction::toggled, [&](bool shown){
            params.toolbarPos = shown ? Qt::TopToolBarArea : Qt::NoToolBarArea;
            params.save();
        });
        menuOptions->addAction(a_toolbarShown);

        a_customToolbar = new QAction(QIcon(dir + "../custToolBar_icon.png"), tr("Customize the toolbar"), this);
        QObject::connect(a_customToolbar, SIGNAL(triggered()), this, SLOT(changeToolbar()));
        menuOptions->addAction(a_customToolbar);

        menuOptions->addSeparator();

        OPTION(tabsClosingButton, tr("Display a close button on the tabs"));
        QObject::connect(a_tabsClosingButton, SIGNAL(toggled(bool)), this, SLOT(setTabsClosable(bool)));

        menuOptions->addSeparator();

        OPTION(tabCloseCheck, tr("When closing a tab, warn about unsaved documents"));

        OPTION(windowCloseCheck, tr("When closing the window, warn about unsaved documents"));

        menuOptions->addSeparator();

        a_autosaveDelay = new QAction(tr("Change the autosave delay"), this);
        menuOptions->addAction(a_autosaveDelay);
        QObject::connect(a_autosaveDelay, SIGNAL(triggered()), this, SLOT(editAutosaveDelay()));

        OPTION(globalAutosave, tr("Autosave all tabs at the same time"));
        QObject::connect(a_globalAutosave, SIGNAL(toggled(bool)), this, SLOT(setAutosaveGlobal(bool)));
        setAutosaveGlobal(params.globalAutosave);

        menuOptions->addSeparator();

        OPTION(darkTheme, tr("Use dark theme"));
        QObject::connect(a_darkTheme, SIGNAL(toggled(bool)), this, SLOT(setDarkTheme(bool)));
        setDarkTheme(params.darkTheme);
    }

    // Help
    {
        QMenu *menuHelp = menuBar()->addMenu("&?");

        a_help = new QAction(QIcon(DIR + "/resources/help/icon.png"), tr("Help"), this);
        a_help->setToolTip(tr("Help (F1)"));
        a_help->setShortcut(QKeySequence("F1"));
        menuHelp->addAction(a_help);
        QObject::connect(a_help, &QAction::triggered, [&]() { Help(this).exec(); });

        menuHelp->addSeparator();

        QAction *about = new QAction(tr("About GpaWa Editor"), this);
        menuHelp->addAction(about);
        QObject::connect(about, &QAction::triggered, [&]() { About(this).exec(); });

        QAction *about_Qt = new QAction(tr("About Qt"), this);
        menuHelp->addAction(about_Qt);
        QObject::connect(about_Qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    }


    toolbarActions[0] = a_new;
    toolbarActions[1] = a_open;
    toolbarActions[2] = a_save;
    toolbarActions[3] = a_saveAs;
    toolbarActions[4] = a_saveAll;
    toolbarActions[5] = a_closeTab;
    toolbarActions[6] = toolbar->addWidget(a_fonts);
    toolbarActions[7] = toolbar->addWidget(a_size);
    toolbarActions[8] = a_changeColor;
    toolbarActions[9] = a_changeBackgroundColor;
    toolbarActions[10] = a_bold;
    toolbarActions[11] = a_italic;
    toolbarActions[12] = a_underline;
    toolbarActions[13] = a_strike;
    toolbarActions[14] = a_upper;
    toolbarActions[15] = a_lower;
    toolbarActions[16] = a_hr;
    toolbarActions[17] = a_help;

    updateToolbar();
}


void GWEMainWin::updateToolbar()
{
    toolbar->clear();
    for(uint i = 0 ; i < NB_TOOLBAR_WIDGETS ; i++)
        if(params.displayTBActions[i])
            toolbar->addAction(toolbarActions[i]);
}

void GWEMainWin::enableToolbarWidgets(bool enable)
{
    // starts at 2 because we want a_new and a_open to be always active, and stops at NB_TOOLBAR_WIDGETS - 1 because we don't want the help button the be disabled
    for(uint i = 2 ; i < NB_TOOLBAR_WIDGETS - 1 ; i++)
        toolbarActions[i]->setEnabled(enable);

    a_selectedTextFont->setEnabled(enable);
}

void GWEMainWin::setDarkTheme(bool dark)
{
    if(dark)
    {
        QFile file(":qdarkstyle/style.qss");
        file.open(QFile::ReadOnly | QFile::Text);
        QTextStream stream(&file);
        qApp->setStyleSheet(stream.readAll());
        file.close();
    }
    else
        qApp->setStyleSheet("");
}

void GWEMainWin::changeToolbar()
{
    ToolbarCustomizer toolbarPopup(this, params.displayTBActions.data());
    Frame** frames = toolbarPopup.getFrames();

    for(uint i = 0 ; i < NB_TOOLBAR_WIDGETS ; i++)
        QObject::connect(frames[i], SIGNAL(changeSetting(std::uint8_t)), this, SLOT(toolbarChanged(std::uint8_t)));

    toolbarPopup.exec();

    for(uint i = 0 ; i < NB_TOOLBAR_WIDGETS ; i++)
        QObject::disconnect(frames[i], SIGNAL(changeSetting(std::uint8_t)), this, SLOT(toolbarChanged(std::uint8_t)));

    saveParams();
}

void GWEMainWin::toolbarChanged(std::uint8_t index)
{
    auto& displayTBActions = params.displayTBActions;
    displayTBActions[index] = !displayTBActions[index];

    if(displayTBActions[index])
        updateToolbar();
    else
        toolbar->removeAction(toolbarActions[index]);
}



void GWEMainWin::saveParams()
{
    params.search_minLength = SearchWindow::minimumLength;
    const bool maxxed = windowState() == Qt::WindowMaximized;
    params.width = maxxed ? oldWidth : width();
    params.height = maxxed ? oldHeight : height();
    params.maximized = maxxed;
    params.toolbarPos = toolBarArea(toolbar);

    if(!params.save())
        QMessageBox::warning(this, tr("Error"), tr("The parameters could not be saved."));
}

