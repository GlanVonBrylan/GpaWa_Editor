<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>About</name>
    <message>
        <location filename="about.cpp" line="33"/>
        <source>&lt;a rel=&quot;license&quot; href=&quot;http://www.gnu.org/licenses/lgpl-3.0.en.html&quot;&gt;http://www.gnu.org/licenses/lgpl-3.0.en.html&lt;/a&gt;&lt;br /&gt;Dark Theme: &lt;a href=&quot;https://github.com/ColinDuquesnoy/QDarkStyleSheet&quot;&gt;QDarkStyleSheet&lt;/a&gt;</source>
        <oldsource>&lt;a rel=&quot;license&quot; href=&quot;http://www.gnu.org/licenses/lgpl-3.0.en.html&quot;&gt;http://www.gnu.org/licenses/lgpl-3.0.en.html&lt;/a&gt;</oldsource>
        <comment>Replace the &apos;.en.html&apos; by the appropriate (for instance &apos;.fr.html&apos;)</comment>
        <translation>&lt;a rel=&quot;license&quot; href=&quot;http://www.gnu.org/licenses/lgpl-3.0.fr.html&quot;&gt;http://www.gnu.org/licenses/lgpl-3.0.fr.html&lt;/a&gt;&lt;br /&gt;Thème sombre : &lt;a href=&quot;https://github.com/ColinDuquesnoy/QDarkStyleSheet&quot;&gt;QDarkStyleSheet&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="about.cpp" line="38"/>
        <source>About GpaWa Editor</source>
        <translation>À propos de GpaWa Editor</translation>
    </message>
    <message>
        <location filename="about.cpp" line="41"/>
        <source>/resources/about.png</source>
        <translation>/resources/a propos.png</translation>
    </message>
</context>
<context>
    <name>Editor</name>
    <message>
        <location filename="editor.cpp" line="181"/>
        <source>Saving error</source>
        <translation>Erreur d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="181"/>
        <source>Saving impossible at
%1
Check that the device %2 has not been removed and that the file is not read-only.</source>
        <translation>Sauvegarde du fichier impossible à l&apos;emplacement
%1
Vérifiez que le périphérique %2 n&apos;a pas été rétiré et que le fichier n&apos;est pas en lecture seule.</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="193"/>
        <source>GWE rich text (*.gwe);;Raw text (*.txt);;HTML rich text (*.htm; *.html);;Other file (*.*)</source>
        <translation>Texte enrichi GWE (*.gwe);;Texte brut (*.txt);;Texte enrichi HTML (*.htm; *.html);;Autre fichier (*.*)</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="194"/>
        <source>Raw text (*.txt);;GWE rich text (*.gwe);;HTML rich text (*.htm; *.html);;Other file (*.*)</source>
        <translation>Texte brut (*.txt);;Texte enrichi GWE (*.gwe);;Texte enrichi HTML (*.htm; *.html);;Autre fichier (*.*)</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="238"/>
        <source>Renaming impossible</source>
        <translation>Renommage impossible</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="238"/>
        <source>&quot;%1&quot; already exists in the same directory.</source>
        <translation>&quot;%1&quot; existe déjà dans le même dossier.</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="251"/>
        <source>Format warning</source>
        <translation>Avertissement sur le format</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="251"/>
        <source>If you switch to *.txt format, you will lose all text formatting. Confirm?</source>
        <translation>Si vous passez au format *.txt, vous perdrez toute mise en forme du texte. Confirmer ?</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="274"/>
        <source>Renaming error</source>
        <translation>Erreur de renommage</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="274"/>
        <source>The file could not be renamed.</source>
        <translation>Le fichier n&apos;a pas pu être renommé.</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="322"/>
        <source>Choose a background color</source>
        <translation>Choisissez une couleur de surlignage</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="191"/>
        <source>Save a file</source>
        <translation>Enregistrer un fichier</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="309"/>
        <source>Choose a color</source>
        <translation>Choisissez une couleur</translation>
    </message>
    <message>
        <location filename="editor.hpp" line="85"/>
        <source>New document</source>
        <translation>Nouveau document</translation>
    </message>
</context>
<context>
    <name>GWEMainWin</name>
    <message>
        <source>New parameters save system</source>
        <translation type="vanished">Nouveau système de sauvegarde des paramètres</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="64"/>
        <source>For an unknown reason, the parameters file (params.set) could not be loaded.</source>
        <translation>Pour une raison inconnue, le fichier de paramètres (params.set) n&apos;a pas pu être chargé.</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="62"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="64"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="70"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="77"/>
        <source>Open all</source>
        <translation>Ouvrir tous</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="79"/>
        <source>Clear list</source>
        <translation>Vider la liste</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="84"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="85"/>
        <source>Save (Ctrl+S)</source>
        <translation>Enregistrer (Ctrl+S)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="89"/>
        <source>Save as...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="90"/>
        <source>Save as... (Ctrl+Shift+S)</source>
        <translation>Enregistrer sous... (Ctrl+Shift+S)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="99"/>
        <source>Save all</source>
        <translation>Enregistrer tout</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="100"/>
        <source>Save all (Ctrl+Alt+S)</source>
        <translation>Enregistrer tout (Ctrl+Alt+S)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="119"/>
        <source>Close the tab</source>
        <translation>Fermer l&apos;onglet</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="120"/>
        <source>Close tab (Ctrl+W)</source>
        <translation>Fermer l&apos;onglet (Ctrl+W)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="125"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="133"/>
        <source>T&amp;ools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="33"/>
        <location filename="gwemainwin_config.cpp" line="136"/>
        <source>Change tabulation size</source>
        <translation>Changer la taille de tabulation</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="33"/>
        <source>How do you want to open this file list?</source>
        <translation>Comment voulez-vous ouvrir cette liste de fichiers ?</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="34"/>
        <source>Do you want to open the files saved in this list, or open this file as it is?</source>
        <translation>Voulez-vous ouvrir les fichiers enregistrés dans cette liste, ou ouvrir la liste telle quelle ?</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="68"/>
        <source>Open files</source>
        <translation>Ouvrir les fichiers</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="69"/>
        <source>Open files and do not ask again</source>
        <translation>Ouvrir les fichiers et ne plus demander</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="70"/>
        <source>Open the list as is</source>
        <translation>Ouvrir la liste telle quelle</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="34"/>
        <source>Enter the tabulation size, in pixels:</source>
        <translation>Entrez la taille de tabulation, en pixels :</translation>
    </message>
    <message>
        <source>GpaWa Editor 6 and higher has a new parameters save system, allowing to make the implementaiton of new parameters easier in the future.
If you updated GpaWa Editor since a version prior to GWE 4.1, this may cause issues. Thank you for your understanding.</source>
        <translation type="vanished">GpaWa Editor 6 et supérieur a un nouveau système d&apos;enregistrement des paramètres qui facilite l&apos;implémentation de nouveaux paramètres à l&apos;avenir.
Si vous mettez GpaWa Editor à jour depuis une version antérieur à GWE 4.1, cela peut causer des problèmes. Merci de votre compréhension.</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="105"/>
        <source>Save as a list</source>
        <translation>Enregistrer comme liste</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="106"/>
        <source>Save as a list (Ctrl+Shift+Alt+S)</source>
        <translation>Enregistrer comme liste (Ctrl+Shift+Alt+S)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="113"/>
        <source>Show file in explorer</source>
        <translation>Voir le fichier dans l&apos;explorateur</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="144"/>
        <source>Selected text&apos;s font</source>
        <translation>Police d&apos;écriture du texte sélectionné</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="149"/>
        <source>Font</source>
        <translation>Police d&apos;écriture</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="154"/>
        <source>Font size</source>
        <translation>Taille de police</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="160"/>
        <source>Selected text&apos;s color</source>
        <translation>Couleur du texte sélectionné</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="161"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="164"/>
        <source>Selected text&apos;s background color</source>
        <translation>Couleur de surlignage du texte sélectionné</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="165"/>
        <source>Background color</source>
        <translation>Couleur de surlignage</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="169"/>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="170"/>
        <source>Bold (Ctrl+B)</source>
        <translation>Gras (Ctrl+B)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="175"/>
        <source>Italic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="176"/>
        <source>Italic (Ctrl+I)</source>
        <translation>Italique (Ctrl+I)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="181"/>
        <source>Underline</source>
        <translation>Souligner</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="182"/>
        <source>Underline (Ctrl+U)</source>
        <translation>Souligner (Ctrl+U)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="187"/>
        <location filename="gwemainwin_config.cpp" line="188"/>
        <source>Strike</source>
        <translation>Barrer</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="192"/>
        <source>Switch the selected text to upper font</source>
        <translation>Passer le texte sélectionné en majuscules</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="195"/>
        <source>Switch the selected text to lower font</source>
        <translation>Passer le texte sélectionné en minuscules</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="198"/>
        <source>Insert an horizontal bar</source>
        <translation>Insérer une barre horizontale</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="199"/>
        <source>Horizontal bar (Ctrl+H)</source>
        <translation>Barre horizontale (Ctrl+H)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="207"/>
        <source>Insert a &amp;list</source>
        <translation>Insérer une &amp;liste</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="209"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="213"/>
        <source>Circles</source>
        <translation>Cercles</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="217"/>
        <source>Squares</source>
        <translation>Carrés</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="221"/>
        <source>Decimal</source>
        <translation>Chiffres</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="225"/>
        <source>Lowercase letters</source>
        <translation>Lettres minuscules</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="229"/>
        <source>Uppercase letters</source>
        <translation>Lettres majuscules</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="233"/>
        <source>Lowercase roman numbers</source>
        <translation>Chiffres romains minuscules</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="237"/>
        <source>Uppercase roman numbers</source>
        <translation>Chiffres romains</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="244"/>
        <source>Search &amp;&amp; replace</source>
        <translation>Chercher &amp;&amp; remplacer</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="257"/>
        <source>Immediately switch to the tab with &quot;New&quot;</source>
        <translation>Basculer immédiatement sur l&apos;onglet avec &quot;Nouveau&quot;</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="259"/>
        <source>Immediately switch to the tab with &quot;Open&quot;</source>
        <translation>Basculer immédiatement sur l&apos;onglet avec &quot;Ouvrir&quot;</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="261"/>
        <source>When opening an already openned file, switch to its tab</source>
        <translation>Lors de l&apos;ouverture d&apos;un fichier déjà ouvert, basculer sur l&apos;onglet</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="263"/>
        <source>Always ask how to open file lists</source>
        <translation>Toujours demander comment ouvrir les listes de fichiers</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="265"/>
        <source>Save documents as rich text by default</source>
        <translation>Enregistrer les documents en texte enrichi par défaut</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="270"/>
        <source>Show full recent files paths</source>
        <translation>Afficher le chemin complet des fichiers récents</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="275"/>
        <source>Show the toolbar</source>
        <translation>Afficher la barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="285"/>
        <source>Customize the toolbar</source>
        <translation>Personnaliser la barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="312"/>
        <source>Use dark theme</source>
        <translation>Utiliser le thème sombre</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="229"/>
        <source>All files (*.*);;Raw text (*.txt);;GWE rich text (*.gwe);;HTML rich text (*.htm; *.html);;All supported rich texts (*.gwe; *.htm; *.html);;File lists (*.gwefl)</source>
        <translation>Tous les fichiers (*.*);;Texte brut (*.txt);;Texte enrichi GWE (*.gwe);;Texte enrichi HTML (*.htm, *.html);;Tous les textes enrichis supportés (*.gwe, *.htm, *.html);;Listes de fichiers (*.gwefl)</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="340"/>
        <source>Save a list</source>
        <translation>Enregistrer une liste</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="340"/>
        <source>File list (*.gwefl)</source>
        <translation>Liste de fichiers (*.gwefl)</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="355"/>
        <source>Saving error</source>
        <translation>Erreur d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="355"/>
        <source>Saving impossible at
%1
Check that the device %2 has not been removed and that the file is not read-only.</source>
        <translation>Sauvegarde du fichier impossible à l&apos;emplacement
%1
Vérifiez que le périphérique %2 n&apos;a pas été rétiré et que le fichier n&apos;est pas en lecture seule.</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="302"/>
        <source>Change the autosave delay</source>
        <translation>Changer le délai de sauvegarde automatique</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="321"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="329"/>
        <source>About GpaWa Editor</source>
        <translation>À propos de GpaWa Editor</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="333"/>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="139"/>
        <source>Nonexistent file</source>
        <translation>Fichier inexistant</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="64"/>
        <location filename="gwemainwin.cpp" line="153"/>
        <location filename="gwemainwin.cpp" line="265"/>
        <location filename="gwemainwin.cpp" line="298"/>
        <location filename="gwemainwin_config.cpp" line="432"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="65"/>
        <source>New (Ctrl+N)</source>
        <translation>Nouveau (Ctrl+N)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="71"/>
        <source>Open (Ctrl+O)</source>
        <translation>Ouvrir (Ctrl+O)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="76"/>
        <source>Open &amp;recent</source>
        <translation>Ouvrir &amp;récent</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="94"/>
        <source>Rename file</source>
        <translation>Renommer le fichier</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="95"/>
        <source>Rename file (Ctrl+R)</source>
        <translation>Renommer le fichier (Ctrl+R)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="140"/>
        <source>Change the default font</source>
        <translation>Police d&apos;écriture par défaut</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="291"/>
        <source>Display a close button on the tabs</source>
        <translation>Afficher un bouton de fermeture sur les onglets</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="296"/>
        <source>When closing a tab, warn about unsaved documents</source>
        <translation>Femeture d&apos;onglet : prévenir des documents non enregistrés</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="298"/>
        <source>When closing the window, warn about unsaved documents</source>
        <translation>Femeture du programme : prévenir des documents non enregistrés</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="306"/>
        <source>Autosave all tabs at the same time</source>
        <translation>Sauvegarde automatique sur tous les onglets à la fois</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="322"/>
        <source>Help (F1)</source>
        <translation>Aide (F1)</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="140"/>
        <source>The file
%1
does not exist anymore. Do you want to create it ?</source>
        <translation>Le fichier
%1
n&apos;existe plus. Voulez-vous le créer ?</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="153"/>
        <source>The file could not be created:
</source>
        <translation>Le fichier n&apos;a pas pu être créé :
</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="222"/>
        <location filename="gwemainwin.cpp" line="265"/>
        <location filename="gwemainwin.cpp" line="298"/>
        <source>Could not open
</source>
        <translation>Impossible d&apos;ouvrir
</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="222"/>
        <source>Openning error</source>
        <translation>Erreur d&apos;ouverture</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="228"/>
        <source>Open a file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="375"/>
        <source>Unsaved modifications</source>
        <translation>Modifications non sauvegardées</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="376"/>
        <source>Some modifications have not been saved.
Do you want to save the file ?</source>
        <translation>Des mofications n&apos;ont pas été sauvegardées.
Voulez-vous enregistrer le fichier ?</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="490"/>
        <location filename="gwemainwin.cpp" line="508"/>
        <source>Choose a font</source>
        <translation>Choisissez une police</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="563"/>
        <source>Autosave</source>
        <translation>Sauvegarde automatique</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="564"/>
        <source>Set the delay between two autosaves in minutes.
(0 = no autosave)</source>
        <translation>Entrez le délai en minutes entre deux sauvegardes
automatiques (0 = pas de sauvegarde auto)</translation>
    </message>
    <message>
        <location filename="gwemainwin_config.cpp" line="432"/>
        <source>The parameters could not be saved.</source>
        <translation>Les paramètres n&apos;ont pas pu être sauvegardés.</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="670"/>
        <source>The document &quot;%1&quot; have not been saved.
Do you want to save it?</source>
        <translation>Le document &quot;%1&quot; n&apos;a pas été enregistré.
Voulez-vous l&apos;enregistrer ?</translation>
    </message>
    <message>
        <location filename="gwemainwin.cpp" line="671"/>
        <source>Unsaved document</source>
        <translation>Document non sauvegardé</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="help.cpp" line="35"/>
        <location filename="help.cpp" line="98"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="help.cpp" line="35"/>
        <location filename="help.cpp" line="114"/>
        <source>Supported formats</source>
        <translation>Formats supportés</translation>
    </message>
    <message>
        <location filename="help.cpp" line="35"/>
        <location filename="help.cpp" line="131"/>
        <source>Autosave</source>
        <translation>Sauvegarde automatique</translation>
    </message>
    <message>
        <location filename="help.cpp" line="35"/>
        <source>Search &amp;&amp; replace</source>
        <translation>Chercher &amp;&amp; remplacer</translation>
    </message>
    <message>
        <location filename="help.cpp" line="35"/>
        <location filename="help.cpp" line="165"/>
        <source>File lists</source>
        <translation>Listes de fichiers</translation>
    </message>
    <message>
        <location filename="help.cpp" line="36"/>
        <source>Okay, thanks!</source>
        <translation>D&apos;accord, merci !</translation>
    </message>
    <message>
        <location filename="help.cpp" line="38"/>
        <source>GpaWa Editor help</source>
        <translation>Aide de GpaWa Editor</translation>
    </message>
    <message>
        <location filename="help.cpp" line="40"/>
        <source>need help.png</source>
        <comment>The &apos;need help&apos; image. Leave it to &apos;need help.png&apos; if there is is not a more fitting image.</comment>
        <translation>besoin d&apos;aide.png</translation>
    </message>
    <message>
        <location filename="help.cpp" line="89"/>
        <source>All keyboard shortcut are indicated in the menus!
The classic ones are also available: Ctrl+C to copy, Ctrl+X to cut, Ctrl+V to paste, Ctrl+Shift+V to paste without formatting, Ctrl+Z to cancel and Ctrl+Y to redo.

In order to set GpaWa Editor as default program: right click on the file of the type you want, click on Properties, then on &quot;Change...&quot;. Finally click on the &quot;Browse...&quot; button and select GpaWa_Editor.exe where you put it.
(on Windows 8 et 10, you will not see the &quot;Browse...&quot; button immediately. Instead, scroll down, click on &quot;More options&quot;, scroll down again, click on &quot;Look for other applications on this PC&quot;, and finally select GpaWa Editor.exe)

If you got any issues, start by reading the &quot;Read me&quot; file which is in the same folder as the executable. If that does not help you, or if you find a bug, you can tell the developper at &lt;a href=&apos;mailto:julesr1230@gmail.com&apos;&gt;julesr1230@gmail.com&lt;/a&gt;
If you want to report a bug, please describe the cirumstances under which it occurs; it is imperative that I reproduce the bug in order to fix it!</source>
        <translation>Les raccourcis clavier sont indiqués dans les menus !
Les classiques sont aussi disponibles : Ctrl+C pour copier, Ctrl+X pour couper, Ctrl+V pour coller, Ctrl+Shift+V pour coller sans formatage, Ctrl+Z pour annuler et Ctrl+Y pour refaire.

Pour mettre GpaWa Editor en programme par défaut : faites clic droit sur un fichier du type que vous désirez, cliquez sur Propriétés, puis cliquez sur le bouton &quot;Modifier...&quot;. Enfin cliquez sur le bouton &quot;Parcourir...&quot; et sélectionnez GpaWa_Editor.exe là où vous l&apos;avez mis.
(sous Windows 8 et 10, vous ne verrez pas le bouton &quot;Parcourir&quot; immédiatement. À la place, descendez dans la fenêtre, cliquez sur &quot;Plus d&apos;options&quot;, descendez encore, cliquez sur &quot;Rechercher d&apos;autres applications sur ce PC&quot;, et enfin sélectionnez GpaWa Editor.exe)

Si vous avez un problème, commencez par lire le fichier &quot;Lisez-moi&quot; qui est dans le même dossier que l&apos;exécutable. Si cela ne vous aide pas, ou que vous avez trouvé un bug, informez-en le développeur à &lt;a href=&apos;mailto:julesr1230@gmail.com&apos;&gt;julesr1230@gmail.com&lt;/a&gt;
Si vous désirez signaler un bug, merci de décrire les circonstance dans lesquelle il arrive ; il est impératif que je puisse le reproduire pour le corriger !</translation>
    </message>
    <message>
        <location filename="help.cpp" line="107"/>
        <source>GpaWa Editor provides three different text formats:

First of all, &lt;i&gt;raw text&lt;/i&gt; (*.txt): as the name implies, it only contains text as such. Any formatting, such as the font size, underlining or lists, is lost when saving as raw text. Special elements like horizontal bars will also be lost. However, the file will be lighter.

Then there is &lt;i&gt;rich text&lt;/i&gt;, which allows to keep formatting, and which is itself provided with the extension *.gwe, *.htm and *.html. The three actually are HTML4, but the *.gwe extension is provided because the two others are rather used for web pages.

At last, there is &lt;i&gt;&quot;Other file&quot;&lt;/i&gt;. GWE allows you to save text under any extension you like (which you will have to specify).
Please note that only the *.gwe, *.htm ans *.html extensions allow saving rich text, any other will output raw text.</source>
        <oldsource>GpaWa Editor provides three different text formats:

First of all, &lt;i&gt;raw text&lt;/i&gt; (*.txt): as the name implies, it only contains text as such. Any formatting, such as the font size, underlining or lists, is lost when saving as raw text. Special elements like horizontal bars will also be lost.

Then there is &lt;i&gt;rich text&lt;/i&gt;, which allows to keep formatting, and which is itself provided with the extension *.gwe, *.htm and *.html. The three actually are HTML4, but the *.gwe extension is provided because the two others are rather used for web pages.

At last, there is &lt;i&gt;&quot;Other file&quot;&lt;/i&gt;. GWE allows you to save text under any extension you like (which you will have to specify).
Please note that only the *.gwe, *.htm ans *.html extensions allow saving rich text, any other will output raw text.</oldsource>
        <translation>GpaWa Editor propose trois formats de texte différents :

Tout d&apos;abord, le &lt;i&gt;texte brut&lt;/i&gt; (*.txt) : comme son nom l&apos;indique, il ne contient que du texte tel quel. Toute mise en forme, comme la taille de police, le soulignage ou les listes, est perdue lors d&apos;un enregistrement en texte brut. Cependant, le fichier sera plus léger.

Ensuite, le &lt;i&gt;texte enrichi&lt;/i&gt;, qui permet de conserver la mise en forme, et qui est lui-même est proposé avec les extension *.gwe, *.htm et *.html. Les trois sont en réalité du HTML4, mais l&apos;extension *.gwe est fournie car les deux autres sont plutôt utilisées pour les pages web.

Enfin, il y a &lt;i&gt;&quot;Autre fichier&quot;&lt;/i&gt;. GWE vous permet d&apos;enregistrer du texte sous l&apos;extension qui vous plaira (que vous devez spécifier).
Veuillez noter que seules les extension *.gwe, *.htm et *.html permettent l&apos;enregistrement de texte enrichi, toute autre extension produira du texte brut.</translation>
    </message>
    <message>
        <location filename="help.cpp" line="123"/>
        <source>GpaWa Editor provides an autosave system.

There are two autosave modes: &lt;i&gt;global&lt;/i&gt; and &lt;i&gt;individual&lt;/i&gt;.
In &lt;i&gt;global&lt;/i&gt; mode (when &quot;Autosave all tabs at the same time&quot; is checked), all tabs autosave at the same time every x minutes, where x is the delay you have set (by default 5).
In &lt;i&gt;individual&lt;/i&gt; mode (by default), each tab has its own countdown and autosaves separately from the others.

Please note that tabs without a file associated (i.e. if they are &quot;New documents&quot;) will not autosave.

If you want to disable autosave, simply set the delay to 0.</source>
        <translation>GpaWa Editor fournit un système de sauvegarde automatique.

Il y a deux modes de sauvegarde automatique : &lt;i&gt;global&lt;/i&gt; et &lt;i&gt;individuel&lt;/i&gt;.
En mode &lt;i&gt;global&lt;/i&gt; (quand &quot;Sauvegarde automatique sur tous les onglets à la fois&quot; est cochée), tous les onglets sauvegardent automatiquement en même temps toutes les x minutes, où x est le délai que vous avez réglé (par défaut 5).
En mode &lt;i&gt;individuel&lt;/i&gt; (le mode par défaut), chaque onglet a son propre compte à rebours et sauvegarde automatiquement séparément des autres.

Veuillez noter que les onglets sans fichier associés (c-à-d les &quot;Nouveaux documents&quot;) ne sauvegardent pas automatiquement.

Si vous voulez désactiver la sauvegarde automatique, mettez simplement le délai à 0.</translation>
    </message>
    <message>
        <location filename="help.cpp" line="141"/>
        <source>GpaWa Editor provides a &quot;Search &amp;amp; replace&quot; feature.&lt;br /&gt;&lt;br /&gt;
Simply press Ctrl+F to open the &quot;Search &amp;amp; replace&quot; window. It displays the following fields:
&lt;ul&gt;&lt;li&gt;&lt;b&gt;Search field:&lt;/b&gt; type here the term you want the search. Then use the arrows the look for the previous or next occurence.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;&quot;Case sensitive&quot; checkbox:&lt;/b&gt; if the search is not case sensitive, it will ignore upper and lower case; for instance, searching &quot;GpaWa&quot; will also highlight occurences sush as &quot;gpawa&quot; or &quot;gPAWA&quot;. A case-sensitive search, however, will always look for the exact term.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Replace field:&lt;/b&gt; type here what you want the occurences to be replaced with. You can leave it empty if you want the occurences to be deleted. Then click on &quot;Replace&quot; the replace the selected occurence. The next one will automatically be selected. You can also click on &quot;Replace all&quot; to replace all the occurences at once.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;&quot;Minimum length&quot; spinbox:&lt;/b&gt; set here the minimum length for the search term to allow searching. This option exists to avoid expensive and useless search; for instance, if you want to look for &quot;error&quot;, you will first type a &apos;e&apos;. Well, without this option, this would cause a search both heavy (because &apos;e&apos; is the most used letter in english) and useless (since this is not what you want to look for). This options is set by default to 2, but you can raise it up to 5, or lower it to 1 (in order to allow single-letter searches).&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>GpaWa Editor propose une fonction &quot;Chercher &amp;amp; remplacer&quot;.&lt;br /&gt;&lt;br /&gt;
Faites simplement Ctrl+F pour ouvrir la fenêtre &quot;Chercher &amp;amp; remplacer&quot;. Elle affiche les champs suivants :
&lt;ul&gt;&lt;li&gt;&lt;b&gt;Champ de recherche :&lt;/b&gt; entrez ici le terme que vous voulez rechercher. Utilisez ensuite les flèches pour voir les occurrences précédentes ou suivantes.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Boîte à cocher &quot;Sensible à la casse&quot; :&lt;/b&gt; si la recherche n&apos;est pas sensible à la casse, elle ignorera les majuscules et minuscules ; par exemple, chercher &quot;GpaWa&quot; donnera également des occurrences comme &quot;gpawa&quot; ou &quot;gPAWA&quot;. Une recherche sensible à la casse, par contre, cherchera toujours le terme exact.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Champ de remplacement :&lt;/b&gt; Entrez ici ce avec quoi vous voulez remplacer les occurrences. Vous pouvez laisser ce champ vide si vous voulez les supprimer. Cliquez ensuite sur &quot;Remplacer&quot; pour remplacer l&apos;occurrence sélectionnée. La suivante sera automatiquement recherchée. Vous pouvez aussi cliquer sur &quot;Remplacer toutes&quot; pour remplacer toutes les occurrences d&apos;un coup.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Boîte &quot;Longueur minimum&quot; :&lt;/b&gt; réglez ici la longueur minimum d&apos;un terme pour autoriser sa recherche. Cette option existe pour éviter des recherches inutiles et coûteuses ; par exemple, si vous voulez chercher &quot;erreur&quot;, vous allez d&apos;abord écrie un &apos;e&apos;. Eh bien, sans cette option, cela causerait une recherche à la fois lourde (puisque &apos;e&apos; est la lettre la plus courante en français) et inutiles (puisque ce n&apos;est pas ce que vous voulez chercher). Cette option est par défaut à 2, mais vous pouvez la monter jusqu&apos;à 5, ou la baisser à 1 (pour autoriser les recherches de lettres seules).&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="help.cpp" line="150"/>
        <source>Search &amp; replace</source>
        <translation>Chercher &amp; remplacer</translation>
    </message>
    <message>
        <location filename="help.cpp" line="159"/>
        <source>Since 9.0, &quot;file lists&quot; are available.

They are special file that contain the paths to several other files. To create such a file, go to File-&gt;Save as a list; this will allow you to create a file that, when openned, will instead open every file you had in your tabs when you created it.
Let&apos;s say, for instance, that you need an &quot;members&quot;, an &quot;addresses&quot; and a &quot;preferences&quot; files to send invitation for a party, which happens every months or so. You don&apos;t have to open all three files indivually every time. Open them, save a file list that you name, let&apos;s say &quot;party&quot;. Next time, just open &quot;party&quot;, and GWE will open &quot;members&quot;, &quot;addresses&quot; and &quot;preferences&quot; for you.

Important note: the files are saved as &quot;absolute paths&quot;, which means that if you move these files, the file list will not work anymore.</source>
        <translation>Depuis la 9.0, les &quot;listes de fichiers&quot; sont disponibles.

Il s&apos;agit de fichiers spéciaux qui contiennent les chemins d&apos;autres fichiers. Pour créer un tel fichier, allez dans Fichier-&gt;Enregistrer comme liste ; cela vous permettra de créer un fichier qui, quand ouvert, ouvrira à la place les fichiers que vous aviez dans vos onglets au moment de sa création.
Disons, par exemple, que vous avez besoin des fichiers &quot;membres&quot;, &quot;adresses&quot; et &quot;préférences&quot; pour envoyer des invitiations à une fête, qui arrive, mettons, tous les mois. You n&apos;avez pas besoin d&apos;ouvrir ces trois fichiers individuellement à chaque fois. Ouvrez-les, enregistrez une liste de fichiers que vous nommerez, disons, &quot;fête&quot;. La prochaine fois, ouvrez simplement &quot;fête&quot;, et GWE ouvrira &quot;membres&quot;, &quot;adresses&quot; et &quot;préférences&quot; pour vous.

Note importante : les fichiers sont enregistrés en tant que &quot;chemins absolus&quot;, ce qui signifie que vous vous déplacez les fichiers, la liste de fichiers ne fonctionnera plus.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="editor.cpp" line="52"/>
        <source>New document</source>
        <translation>Nouveau document</translation>
    </message>
    <message>
        <location filename="recentfileslist.cpp" line="80"/>
        <location filename="recentfileslist.cpp" line="217"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="recentfileslist.cpp" line="80"/>
        <source>The recent files list could not be loaded.</source>
        <translation>La liste des fichiers récents n&apos;a pas pu être chargée.</translation>
    </message>
    <message>
        <location filename="recentfileslist.cpp" line="217"/>
        <source>The recent files list could not be saved.</source>
        <translation>La liste des fichiers récent n&apos;a pas pu être sauvegardée.</translation>
    </message>
</context>
<context>
    <name>RenameWindow</name>
    <message>
        <location filename="renamewindow.cpp" line="35"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="renamewindow.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="renamewindow.cpp" line="37"/>
        <source>Rename file</source>
        <translation>Renommer le fichier</translation>
    </message>
</context>
<context>
    <name>SearchWindow</name>
    <message>
        <location filename="searchwindow.cpp" line="42"/>
        <source>Case-sensitive</source>
        <translation>Sensible à la casse</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="44"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="45"/>
        <source>Replace all</source>
        <translation>Remplacer toutes</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="47"/>
        <source>Minimum length of the search term:</source>
        <translation>Longueur minimum du terme recherché :</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="48"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="53"/>
        <source>Search &amp; replace</source>
        <translation>Chercher &amp; remplacer</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="57"/>
        <source>Close the search window</source>
        <translation>Fermer la fenêtre de recherche</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="59"/>
        <source>Search...</source>
        <translation>Chercher...</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="62"/>
        <source>Search the previous one</source>
        <translation>Chercher la précédente</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="66"/>
        <source>Search the next one</source>
        <translation>Chercher la suivante</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="69"/>
        <source>Replace by...</source>
        <translation>Remplacer par...</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="155"/>
        <source>Search term too short</source>
        <translation>Terme recherché trop court</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="197"/>
        <source>Search term not found</source>
        <translation>Terme recherché introuvable</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="222"/>
        <location filename="searchwindow.cpp" line="293"/>
        <source>Back to the beginning of the document</source>
        <translation>Retour au début du document</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="251"/>
        <source>Back to the end of the document</source>
        <translation>Retour à la fin du document</translation>
    </message>
    <message>
        <location filename="searchwindow.cpp" line="328"/>
        <source>Search error</source>
        <translation>Erreur de recherche</translation>
    </message>
    <message numerus="yes">
        <location filename="searchwindow.cpp" line="329"/>
        <source>An error has occured while trying to search :
current is null, %n found.
Please report this error to the developper.
Giving them the text you were searching and the one in which you were searching it would help.
If you could even tell the position of you cursor, it would be awesome ! (even if approximate)</source>
        <translation>
            <numerusform>Une erreur est survenue en tentant d&apos;effectuer la recherche&amp;nbsp;:
current est null, %n trouvée.
Veuillez signaler cette erreur au développeur.
Lui donner le terme rechercher et le texte dans lequel vous le recherchiez aiderait.
Si vous pouviez même donner la position du curseur, ce serait formidable ! (même approximativement)</numerusform>
            <numerusform>Une erreur est survenue en tentant d&apos;effectuer la recherche&amp;nbsp;:
current est null, %n trouvées.
Veuillez signaler cette erreur au développeur.
Lui donner le terme rechercher et le texte dans lequel vous le recherchiez aiderait.
Si vous pouviez même donner la position du curseur, ce serait formidable ! (même approximativement)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Tabs</name>
    <message>
        <location filename="tabs.cpp" line="56"/>
        <source>Copy the full path</source>
        <translation>Copier le chemin complet</translation>
    </message>
    <message>
        <location filename="tabs.cpp" line="60"/>
        <source>Show file in explorer</source>
        <translation>Voir le fichier dans l&apos;explorateur</translation>
    </message>
    <message>
        <location filename="tabs.cpp" line="66"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="tabs.cpp" line="70"/>
        <source>Close others</source>
        <translation>Fermer les autres</translation>
    </message>
</context>
<context>
    <name>ToolbarCustomizer</name>
    <message>
        <location filename="toolbarcustomizer.cpp" line="41"/>
        <source>Customize the toolbar</source>
        <translation>Personnaliser la barre d&apos;outils</translation>
    </message>
</context>
</TS>
