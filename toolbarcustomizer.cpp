// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "toolbarcustomizer.hpp"

#include <QMouseEvent>


void Frame::mouseReleaseEvent(QMouseEvent* event)
{
    if(rect().contains(event->pos()))
        checkBox->toggle();
}


ToolbarCustomizer::ToolbarCustomizer(QWidget *parent, bool *values) : QDialog(parent, Qt::MSWindowsFixedSizeDialogHint)
{
    setWindowTitle(tr("Customize the toolbar"));
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ok = new QPushButton("Ok");
    QObject::connect(ok, SIGNAL(clicked()), this, SLOT(close()));

    for(uint i = 0 ; i < NB_TOOLBAR_WIDGETS ; i++)
    {
        labels[i] = new QLabel();
        frames[i] = new Frame(i, new QCheckBox(labels[i]));
        fLayouts[i] = new QHBoxLayout();

        frames[i]->checkBox->setChecked(values[i]);
        frames[i]->setFrameShape(QFrame::StyledPanel);
    }

    const QString dirF(DIR + "/resources/file/");
    const QString dirT(DIR + "/resources/tools/");

    labels[0]->setPixmap(QPixmap(dirF + "new.png"));
    labels[1]->setPixmap(QPixmap(dirF + "open.png"));
    labels[2]->setPixmap(QPixmap(dirF + "save.png"));
    labels[3]->setPixmap(QPixmap(dirF + "saveAs.png"));
    labels[4]->setPixmap(QPixmap(dirF + "saveAll.png"));
    labels[5]->setPixmap(QPixmap(dirT + "closeTab.png"));
    labels[6]->setPixmap(QPixmap(dirT + "custToolBar_fontComboBox.png"));
    labels[7]->setPixmap(QPixmap(dirT + "custToolBar_fontSize.png"));
    labels[8]->setPixmap(QPixmap(dirT + "text/color.png"));
    labels[9]->setPixmap(QPixmap(dirT + "text/color_background.png"));
    labels[10]->setPixmap(QPixmap(dirT + "text/bold.png"));
    labels[11]->setPixmap(QPixmap(dirT + "text/italic.png"));
    labels[12]->setPixmap(QPixmap(dirT + "text/underline.png"));
    labels[13]->setPixmap(QPixmap(dirT + "text/strike.png"));
    labels[14]->setPixmap(QPixmap(dirT + "text/case_upper.png"));
    labels[15]->setPixmap(QPixmap(dirT + "text/case_lower.png"));
    labels[16]->setPixmap(QPixmap(dirT + "hr.png"));
    labels[17]->setPixmap(QPixmap(DIR + "/resources/help/icon.png"));

    for(uint i = 0 ; i < NB_TOOLBAR_WIDGETS ; i++)
    {
        fLayouts[i]->addWidget(labels[i]);
        fLayouts[i]->addWidget(frames[i]->checkBox);
        frames[i]->setLayout(fLayouts[i]);
    }

    // In the following bloc, labels[x] are NOT in the right order. And this is normal.
    // This allows a more beautiful icons arrangement.
    layout = new QGridLayout();

    layout->addWidget(frames[0], 0, 0);
    layout->addWidget(frames[1], 0, 1);
    layout->addWidget(frames[2], 0, 2);

    layout->addWidget(frames[3], 1, 0);
    layout->addWidget(frames[4], 1, 1);
    layout->addWidget(frames[5], 1, 2);

    layout->addWidget(frames[6], 2, 0, 1, 2);
    layout->addWidget(frames[7], 2, 2);

    layout->addWidget(frames[8], 3, 0);
    layout->addWidget(frames[9], 3, 1);
    layout->addWidget(frames[14], 3, 2);

    layout->addWidget(frames[10], 4, 0);
    layout->addWidget(frames[11], 4, 1);
    layout->addWidget(frames[15], 4, 2);

    layout->addWidget(frames[12], 5, 0);
    layout->addWidget(frames[13], 5, 1);
    layout->addWidget(frames[16], 5, 2);

    layout->addWidget(frames[17], 6, 1);

    layout->addWidget(ok, 7, 2);

    setLayout(layout);

    resize(300, height());
}

ToolbarCustomizer::~ToolbarCustomizer()
{
    for(uint i = 0 ; i < NB_TOOLBAR_WIDGETS ; i++)
    {
        delete labels[i];
        delete frames[i];
    }
}

Frame** ToolbarCustomizer::getFrames()
{
    return frames;
}
