// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////

#include "tabs.hpp"
#include "editor.hpp"
#include <QMenu>

Tabs::Tabs()
{
    QTabWidget();
    this->setTabBar(new TabBar());
}

#define ifFileConnect(action, slot) if(editor->hasFile()) connect(&action, SIGNAL(triggered()), editor, SLOT(slot())); else action.setEnabled(false);

void Tabs::TabBar::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::MiddleButton)
        emit qobject_cast<Tabs*>(this->parentWidget())->tabCloseRequested(this->tabAt(event->pos()));
    else if(event->button() == Qt::RightButton)
        qobject_cast<Tabs*>(this->parentWidget())->showMenuFor(tabAt(event->pos()));

    QTabBar::mouseReleaseEvent(event);
}

void Tabs::showMenuFor(int index)
{
    QWidget* tab = widget(index);
    if(tab)
    {
        Editor* editor = tab->findChild<Editor*>();
        QMenu menu;

        QAction copyPath(tr("Copy the full path"), &menu);
        ifFileConnect(copyPath, copyFullPath)
        menu.addAction(&copyPath);

        QAction showInExplorer(tr("Show file in explorer"), &menu);
        ifFileConnect(showInExplorer, showInExplorer)
        menu.addAction(&showInExplorer);

        menu.addSeparator();

        QAction close(tr("Close"), &menu);
        connect(&close, &QAction::triggered, this, [index, this](){ emit this->tabCloseRequested(index); });
        menu.addAction(&close);

        QAction closeOthers(tr("Close others"), &menu);
        if(count() == 1)
            closeOthers.setEnabled(false);
        else
            connect(&closeOthers, &QAction::triggered, this, [index, this](){ emit this->closeAllExcept(index); });
        menu.addAction(&closeOthers);

        menu.exec(QCursor::pos());
    }
}
