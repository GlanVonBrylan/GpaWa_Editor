// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "help.hpp"


Help::Help(QWidget *parent)
    : QDialog(parent),
        title(), titleImage(),
        mainLayout(),
        text(),
        general(tr("General")), formats(tr("Supported formats")), autosave(tr("Autosave")), search(tr("Search && replace")), fileLists(tr("File lists")),
        current(&formats), ok(tr("Okay, thanks!"))
{
    setWindowTitle(tr("GpaWa Editor help"));
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    titleImage.setPixmap(QPixmap(DIR + "/resources/help/" + tr("need help.png", "The 'need help' image. Leave it to 'need help.png' if there is is not a more fitting image.")));
    titleImage.setMaximumHeight(30);
    resize(qBound(500, (int)(parent->width() * 0.75), 1000), qBound(300, (int)(parent->height() * 0.75), 500));

    titleImage.setAlignment(Qt::AlignRight);

    QFont font;
    font.setPointSize(14);
    title.setFont(font);
    font.setPointSize(10);
    text.setFont(font);
    text.setMinimumWidth(450);
    text.setOpenExternalLinks(true);

    QObject::connect(&general, SIGNAL(clicked()), this, SLOT(displayGeneral()));
    QObject::connect(&formats, SIGNAL(clicked()), this, SLOT(displayFormats()));
    QObject::connect(&autosave, SIGNAL(clicked()), this, SLOT(displayAutosave()));
    QObject::connect(&search, SIGNAL(clicked()), this, SLOT(displaySearch()));
    QObject::connect(&fileLists, SIGNAL(clicked()), this, SLOT(displayFileLists()));
    QObject::connect(&ok, SIGNAL(clicked()), this, SLOT(close()));

    general.setAutoDefault(false);
    formats.setAutoDefault(false);
    autosave.setAutoDefault(false);
    search.setAutoDefault(false);
    ok.setDefault(true);

    mainLayout.addWidget(&title, 0, 0, 1, 3);
    mainLayout.addWidget(&titleImage, 0, 5, 1, 2);

    mainLayout.addWidget(&general, 1, 6);
    mainLayout.addWidget(&formats, 2, 6);
    mainLayout.addWidget(&autosave, 3, 6);
    mainLayout.addWidget(&search, 4, 6);
    mainLayout.addWidget(&fileLists, 5, 6);

    mainLayout.addWidget(&text, 1, 0, HELP_CATEGORIES + 2, 6);
    mainLayout.addWidget(&ok, HELP_CATEGORIES + 2, 6);
    setLayout(&mainLayout);

    displayGeneral();
}

void Help::displayGeneral()
{
    general.setEnabled(false);
    current->setEnabled(true);
    current = &general;

    QString help(tr("All keyboard shortcut are indicated in the menus!\n"
                    "The classic ones are also available: Ctrl+C to copy, Ctrl+X to cut, Ctrl+V to paste, Ctrl+Shift+V to paste without formatting, Ctrl+Z to cancel and Ctrl+Y to redo.\n\n"
                    "In order to set GpaWa Editor as default program: right click on the file of the type you want, click on Properties, then on \"Change...\". Finally click on the \"Browse...\" button and select GpaWa_Editor.exe where you put it.\n"
                    "(on Windows 8 et 10, you will not see the \"Browse...\" button immediately. Instead, scroll down, click on \"More options\", scroll down again, click on \"Look for other applications on this PC\", and finally select GpaWa Editor.exe)\n\n"
                    "If you got any issues, start by reading the \"Read me\" file which is in the same folder as the executable. If that does not help you, or if you find a bug, you can tell the developper at <a href='mailto:julesr1230@gmail.com'>julesr1230@gmail.com</a>\n"
                    "If you want to report a bug, please describe the cirumstances under which it occurs; it is imperative that I reproduce the bug in order to fix it!")
                 .replace("<a", "<a style=\"color: #2A78CC;\""));

    text.setHtml(help.replace("&nbsp;", "\u00A0").replace("\n", "<br />"));
    title.setText(tr("General"));
}

void Help::displayFormats()
{
    formats.setEnabled(false);
    current->setEnabled(true);
    current = &formats;

    QString help(tr("GpaWa Editor provides three different text formats:\n\n"
                    "First of all, <i>raw text</i> (*.txt): as the name implies, it only contains text as such. Any formatting, such as the font size, underlining or lists, is lost when saving as raw text. Special elements like horizontal bars will also be lost. However, the file will be lighter.\n\n"
                    "Then there is <i>rich text</i>, which allows to keep formatting, and which is itself provided with the extension *.gwe, *.htm and *.html. The three actually are HTML4, but the *.gwe extension is provided because the two others are rather used for web pages.\n\n"
                    "At last, there is <i>\"Other file\"</i>. GWE allows you to save text under any extension you like (which you will have to specify).\n"
                    "Please note that only the *.gwe, *.htm ans *.html extensions allow saving rich text, any other will output raw text."));

    text.setHtml(help.replace("&nbsp;", "\u00A0").replace("\n", "<br />"));
    title.setText(tr("Supported formats"));
}

void Help::displayAutosave()
{
    autosave.setEnabled(false);
    current->setEnabled(true);
    current = &autosave;

    QString help(tr("GpaWa Editor provides an autosave system.\n\n"
                    "There are two autosave modes: <i>global</i> and <i>individual</i>.\n"
                    "In <i>global</i> mode (when \"Autosave all tabs at the same time\" is checked), all tabs autosave at the same time every x minutes, where x is the delay you have set (by default 5).\n"
                    "In <i>individual</i> mode (by default), each tab has its own countdown and autosaves separately from the others.\n\n"
                    "Please note that tabs without a file associated (i.e. if they are \"New documents\") will not autosave.\n\n"
                    "If you want to disable autosave, simply set the delay to 0."));

    text.setHtml(help.replace("&nbsp;", "\u00A0").replace("\n", "<br />"));
    title.setText(tr("Autosave"));
}

void Help::displaySearch()
{
    search.setEnabled(false);
    current->setEnabled(true);
    current = &search;

    // Here <br /> are placed directly instead of having them replacing '\n' characters, because otherwise the entire <ul> section was on a single line, which is horrible to translate.
    QString help(tr("GpaWa Editor provides a \"Search &amp; replace\" feature.<br /><br />\n"
                    "Simply press Ctrl+F to open the \"Search &amp; replace\" window. It displays the following fields:\n"
                    "<ul><li><b>Search field:</b> type here the term you want the search. Then use the arrows the look for the previous or next occurence.</li>\n"
                    "<li><b>\"Case sensitive\" checkbox:</b> if the search is not case sensitive, it will ignore upper and lower case; for instance, searching \"GpaWa\" will also highlight occurences sush as \"gpawa\" or \"gPAWA\". A case-sensitive search, however, will always look for the exact term.</li>\n"
                    "<li><b>Replace field:</b> type here what you want the occurences to be replaced with. You can leave it empty if you want the occurences to be deleted. Then click on \"Replace\" the replace the selected occurence. The next one will automatically be selected. You can also click on \"Replace all\" to replace all the occurences at once.</li>\n"
                    "<li><b>\"Minimum length\" spinbox:</b> set here the minimum length for the search term to allow searching. This option exists to avoid expensive and useless search; for instance, if you want to look for \"error\", you will first type a 'e'. "
                        "Well, without this option, this would cause a search both heavy (because 'e' is the most used letter in english) and useless (since this is not what you want to look for). This options is set by default to 2, but you can raise it up to 5, or lower it to 1 (in order to allow single-letter searches).</li></ul>"));

    text.setHtml(help.replace("&nbsp;", "\u00A0"));
    title.setText(tr("Search & replace"));
}

void Help::displayFileLists()
{
    fileLists.setEnabled(false);
    current->setEnabled(true);
    current = &fileLists;

    QString help(tr("Since 9.0, \"file lists\" are available.\n\n"
                    "They are special file that contain the paths to several other files. To create such a file, go to File->Save as a list; this will allow you to create a file that, when openned, will instead open every file you had in your tabs when you created it.\n"
                    "Let's say, for instance, that you need an \"members\", an \"addresses\" and a \"preferences\" files to send invitation for a party, which happens every months or so. You don't have to open all three files indivually every time. Open them, save a file list that you name, let's say \"party\". Next time, just open \"party\", and GWE will open \"members\", \"addresses\" and \"preferences\" for you.\n\n"
                    "Important note: the files are saved as \"absolute paths\", which means that if you move these files, the file list will not work anymore."));

    text.setHtml(help.replace("&nbsp;", "\u00A0").replace("\n", "<br />"));
    title.setText(tr("File lists"));
}
