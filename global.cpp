// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "global.hpp"

#include <QDir>


namespace GWEGlobal
{

    bool logError(const QString& error, const QString& filename)
    {
        const QString errDir(DIR + "/error logs/");
        if(!QDir(errDir).exists())
            QDir().mkdir(errDir);

        QFile logFile(errDir + filename + ".log");
        bool res = logFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);

        if(res)
        {
            QTextStream stream(&logFile);
            stream<<error;
        }

        return res;
    }

} // namespace GWEGlobal
