QT += widgets core gui

CONFIG += c++14

SOURCES += \
    gwemainwin_config.cpp \
    main.cpp \
    gwemainwin.cpp \
    params.cpp \
    tabs.cpp \
    toolbarcustomizer.cpp \
    recentfileslist.cpp \
    help.cpp \
    editor.cpp \
    about.cpp \
    searchwindow.cpp \
    renamewindow.cpp \
    global.cpp

HEADERS += \
    params.hpp \
    renamewindow.hpp \
    recentfileslist.hpp \
    searchwindow.hpp \
    tabs.hpp \
    toolbarcustomizer.hpp \
    macros.hpp \
    help.hpp \
    gwemainwin.hpp \
    editor.hpp \
    about.hpp \
    global.hpp

TRANSLATIONS = gwe_fr.ts

RESOURCES += qdarkstyle/style.qrc

VERSION = 9.5

QMAKE_TARGET_PRODUCT = "GpaWa Editor"
QMAKE_TARGET_DESCRIPTION = "GpaWa Editor"
QMAKE_TARGET_COPYRIGHT = "Copyright (c) 2014-2024 Jules Renton--Epinette"

DEFINES += VERSION=$$VERSION

RC_ICONS = GWEIcon.ico

DISTFILES += \
	todo.txt \
    README.md \
    .gitignore
