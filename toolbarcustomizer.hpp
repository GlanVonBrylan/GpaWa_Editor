#ifndef TOOLBARCUSTOMIZER_H
#define TOOLBARCUSTOMIZER_H

// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include <QDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>

#include "global.hpp"

/**
 * \class Frame
 * Class that holds the check boxes of the toolbar editing window.
 */
class Frame : public QFrame
{
    Q_OBJECT

public:
    Frame(std::uint8_t i, QCheckBox *chkBx)
        : QFrame(nullptr), index(i), checkBox(chkBx)
    {
        QObject::connect(checkBox, SIGNAL(toggled(bool)), this, SLOT(switched()));
    }

    ~Frame()
    {
        delete checkBox;
    }

    const std::uint8_t index;
    QCheckBox *const checkBox;

signals:
    void changeSetting(std::uint8_t i);

public slots:
    void switched() { emit changeSetting(index); }

protected:
    void mouseReleaseEvent(QMouseEvent* event);
};


/**
 * \class ToolbarCustomizer
 * Class that holds the toolbar editing window with real-time modifications.
 */
class ToolbarCustomizer : public QDialog
{
public:
    ToolbarCustomizer(QWidget *parent, bool values[]);

    ~ToolbarCustomizer();

    Frame** getFrames();

private:
    QGridLayout *layout;

    QPushButton *ok;

    QLabel *labels[NB_TOOLBAR_WIDGETS];
    Frame *frames[NB_TOOLBAR_WIDGETS];
    QHBoxLayout *fLayouts[NB_TOOLBAR_WIDGETS];
};

#endif // TOOLBARCUSTOMIZER_H
