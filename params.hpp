#ifndef PARAMS_HPP
#define PARAMS_HPP

// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////

#include "global.hpp"
#include <QFile>


struct Params
{
    const QString FILE_PATH = DIR + "/params.set";

    Params();

    bool fileExists() { return QFile::exists(FILE_PATH); }
    bool load();
    bool save();

    QString defaultFont;
    int width, height;
    bool maximized;
    bool darkTheme = false;

    int autosaveDelay = 300'000; // in ms
    bool globalAutosave = false;

    Qt::ToolBarArea toolbarPos = Qt::TopToolBarArea;
    std::array<bool, NB_TOOLBAR_WIDGETS> displayTBActions;

    bool switchToTabWhenNew = true, switchToTabWhenOpen = true;
    bool tabCloseCheck = true, windowCloseCheck = true;
    bool tabsClosingButton = false;
    bool openTabIfFileOpened = true;
    bool showFullRecentFilesPaths = false;

    // Tabulation size in pixels (Qt default is 80px; GWE default is 40px)
    int tabulationSize = 40;

    bool askHowToOpenFileLists = true;
    bool saveAsRichTextByDefault = true;

    int search_minLength = 2;
};

#endif // PARAMS_HPP
