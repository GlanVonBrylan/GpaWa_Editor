// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "searchwindow.hpp"



int SearchWindow::minimumLength = 2;


SearchWindow::SearchWindow(Editor *associate, QWidget *parent)
    : QDialog(parent),
    partner(associate),
    layout(new QGridLayout()),
    searchField(new QLineEdit()),
    countDisplay(new QLabel("0/0")),
    a_previous(new QToolButton()),
    a_next(new QToolButton()),
    caseSensitive(new QCheckBox(tr("Case-sensitive"))),
    replaceField(new QLineEdit()),
    replaceButton(new QPushButton(tr("Replace"))),
    replaceAllButton(new QPushButton(tr("Replace all"))),
    info(new QLabel()),
    minLengthLabel(new QLabel(tr("Minimum length of the search term:"))), minLength(new QSpinBox()),
    a_close(new QPushButton(tr("Close"))),
    s_prev(QKeySequence("Up"), this, SLOT(searchBackwards())), s_next(QKeySequence("Down"), this, SLOT(search())),
    text(partner->toPlainText()), src(),
    indexes(nullptr), count(0), current(0)
{
    setWindowTitle(tr("Search & replace"));
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    //a_close->setIcon(QIcon(DIR + "/resources/tools/search/close.png"));
    a_close->setToolTip(tr("Close the search window"));

    searchField->setPlaceholderText(tr("Search..."));

    a_previous->setIcon(QIcon(DIR + "/resources/tools/search/previous.png"));
    a_previous->setToolTip(tr("Search the previous one"));
    a_previous->setAutoRaise(true);

    a_next->setIcon(QIcon(DIR + "/resources/tools/search/next.png"));
    a_next->setToolTip(tr("Search the next one"));
    a_next->setAutoRaise(true);

    replaceField->setPlaceholderText(tr("Replace by..."));

    minLength->setMinimum(1);
    minLength->setMaximum(9);
    minLength->setValue(minimumLength);

    replaceButton->setAutoDefault(false);
    replaceAllButton->setAutoDefault(false);
    a_close->setAutoDefault(false);

    searchField->setContentsMargins(3, 0, 3, 0);
    countDisplay->setContentsMargins(3, 0, 3, 0);
    replaceButton->setContentsMargins(3, 0, 3, 0);
    info->setContentsMargins(3, 0, 0, 0);

    layout->addWidget(searchField, 0, 0, 1, 2);
    layout->addWidget(a_previous, 0, 2);
    layout->addWidget(a_next, 0, 3);
    layout->addWidget(countDisplay, 0, 4, 1, 2);
    layout->addWidget(caseSensitive, 1, 0);
    layout->addWidget(info, 1, 2, 1, 4);
    layout->addWidget(replaceField, 2, 0, 1, 2);
    layout->addWidget(replaceButton, 2, 2, 1, 2);
    layout->addWidget(replaceAllButton, 2, 4, 1, 2);
    layout->addWidget(minLengthLabel, 3, 0, 1, 2);
    layout->addWidget(minLength, 3, 2);
    layout->addWidget(a_close, 3, 5);

    setLayout(layout);

    QObject::connect(searchField, SIGNAL(textEdited(const QString&)), this, SLOT(setSearchText(const QString&)));
    QObject::connect(searchField, SIGNAL(returnPressed()), this, SLOT(search()));
    QObject::connect(a_previous, SIGNAL(clicked()), this, SLOT(searchBackwards()));
    QObject::connect(a_next, SIGNAL(clicked()), this, SLOT(search()));
    QObject::connect(caseSensitive, SIGNAL(toggled(bool)), this, SLOT(updateIndexes()));
    QObject::connect(replaceField, SIGNAL(returnPressed()), this, SLOT(replace()));
    QObject::connect(replaceButton, SIGNAL(clicked()), this, SLOT(replace()));
    QObject::connect(replaceAllButton, SIGNAL(clicked()), this, SLOT(replaceAll()));
    QObject::connect(minLength, SIGNAL(valueChanged(int)), this, SLOT(minLengthChanged(int)));
    QObject::connect(a_close, SIGNAL(clicked()), this, SLOT(close()));


    QString selection = partner->textCursor().selectedText();

    if(!selection.isEmpty())
        searchField->setText(selection);

    setSearchText(searchField->text());

    searchField->setFocus();
}

SearchWindow::~SearchWindow()
{
    delete[] indexes;
}



void SearchWindow::minLengthChanged(int len)
{
    const int oldL = minimumLength,
              srcL = src.length();

    minimumLength = len;
    if((srcL < len && srcL >= oldL) || (srcL >= len && srcL < oldL))
        updateIndexes();
}

void SearchWindow::setSearchText(const QString& srcText)
{
    src = srcText;
    updateIndexes();
}

void SearchWindow::updateIndexes()
{
    delete[] indexes;
    indexes = nullptr;
    current = 0;

    if(src.length() < minimumLength)
    {
        count = 0;
        setButtonsDisabled(true);
        countDisplay->setText("0/0");
        info->setText(tr("Search term too short"));
    }
    else
    {
        const Qt::CaseSensitivity caseSens = caseSensitive->checkState() ? Qt::CaseSensitive : Qt::CaseInsensitive;
        count = text.count(src, caseSens);
        setButtonsDisabled(!count);

        if(count)
        {
            indexes = new uint[count];
            indexes[0] = text.indexOf(src, 0, caseSens);

            QTextCursor cursor = partner->textCursor();
            const uint cursorPos = qMin(cursor.anchor(), cursor.position());

            if(indexes[0] >= cursorPos)
                current = 1;

            for(uint i = 1 ; i < count ; i++)
            {
                indexes[i] = text.indexOf(src, indexes[i-1] + 1, caseSens);
                if(!current && indexes[i] >= cursorPos)
                    current = i + 1;
            }

            if(!current) // If the cursor is after all occurences
                current = count;

            if(count == 1)
            {
                a_previous->setDisabled(true);
                a_next->setDisabled(true);
            }

            countDisplay->setText(QString("%1/%2").arg(current).arg(count));
            info->clear();
            selectOccurence(current-1);
        }
        else
        {
            countDisplay->setText("0/0");
            info->setText(tr("Search term not found"));
        }
    }
}

void SearchWindow::setButtonsDisabled(bool dis)
{
    a_previous->setDisabled(dis);
    a_next->setDisabled(dis);
    replaceButton->setDisabled(dis);
    replaceAllButton->setDisabled(dis);
}


int SearchWindow::search()
{
    if(!count)
        return -1;

    if(current)
    {
        current++;
        if(current > count)
        {
            current = 1;
            info->setText(tr("Back to the beginning of the document"));
        }
        else
            info->clear();

        selectOccurence(current-1);

        countDisplay->setText(QString("%1/%2").arg(current).arg(count));
    }
    else
    {
        info->setText("Search error");
        error();
    }

    return current - 1;
}

int SearchWindow::searchBackwards()
{
    if(!count)
        return -1;

    if(current)
    {
        current--;
        if(current == 0)
        {
            current = count;
            info->setText(tr("Back to the end of the document"));
        }
        else
            info->clear();

        selectOccurence(current-1);

        countDisplay->setText(QString("%1/%2").arg(current).arg(count));
    }
    else
    {
        info->setText("Search error");
        error();
    }

    return current - 1;
}

void SearchWindow::selectOccurence(uint index)
{
    const uint pos = indexes[index];
    QTextCursor cursor = partner->textCursor();
    cursor.setPosition(pos);
    cursor.setPosition(pos + src.size(), QTextCursor::KeepAnchor);
    partner->setTextCursor(cursor);
}



bool SearchWindow::replace()
{
    if(partner->textCursor().hasSelection())
    {
        const bool backToBeginning = current == count && count != 1;

        partner->insertPlainText(replaceField->text());
        text = partner->toPlainText();
        updateIndexes();

        if(backToBeginning)
        {
            selectOccurence(0);
            info->setText(tr("Back to the beginning of the document"));
        }

        return true;
    }

    return false;
}

int SearchWindow::replaceAll()
{
    uint times = 0;
    int diff = replaceField->text().length() - searchField->text().length();

    QTextCursor cursor = partner->textCursor();

    while(times < count)
    {
        selectOccurence(times);
        partner->insertPlainText(replaceField->text());

        indexes[times+1] += diff * (times+1);
        times++;
    }

    cursor.setPosition(qMin(cursor.position(), cursor.anchor()) - current * diff);
    partner->setTextCursor(cursor);
    text = partner->toPlainText();
    updateIndexes();

    return times;
}

void SearchWindow::error()
{
    QMessageBox::critical(this, tr("Search error"),
        tr("An error has occured while trying to search :\n"
           "current is null, %n found.\n"
           "Please report this error to the developper.\n"
           "Giving them the text you were searching and the one in which you were searching it would help.\n"
           "If you could even tell the position of you cursor, it would be awesome ! (even if approximate)", "", count));
}

