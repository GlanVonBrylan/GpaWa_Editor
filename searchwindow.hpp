#ifndef SEARCHWINDOW_H
#define SEARCHWINDOW_H

// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include <QToolButton>
#include <QSpinBox>
#include <QShortcut>

#include "editor.hpp"


class GWEMainWin;


class SearchWindow : public QDialog
{
    Q_OBJECT

public:

    static int minimumLength;


    SearchWindow(Editor *associate, QWidget* parent = nullptr);

    ~SearchWindow();


    // Disables or enables all buttons apart from the closing one.
    void setButtonsDisabled(bool dis);

public slots:

    void updateIndexes();

    void minLengthChanged(int len);

    void setSearchText(const QString& srcText);

    // Returns the either the current index' index, or -1 if an error occured.
    int search();
    int searchBackwards();

    void selectOccurence(uint index);

    bool replace();
    int replaceAll();

private:

    void error();

    Editor *const partner;

    QGridLayout *layout;

    QLineEdit *const searchField;
    QLabel *const countDisplay;
    QToolButton *const a_previous, *const a_next;
    QCheckBox *const caseSensitive;
    QLineEdit *const replaceField;
    QPushButton *const replaceButton, *const replaceAllButton;
    QLabel *const info;
    QLabel *const minLengthLabel;
    QSpinBox *const minLength;
    QPushButton *const a_close;

    QShortcut s_prev, s_next;

    QString text;
    QString src;
    uint *indexes;
    uint count;
    uint current; // BEWARE ! current starts at 1, since the value 0 is used to say 'no current', for when the search cannot be done.
};

#endif // SEARCHWINDOW_H
