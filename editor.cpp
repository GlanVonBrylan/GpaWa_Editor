// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "editor.hpp"
#include <QPalette>
#include <QTextDocumentFragment>
#include <QClipboard>

#ifdef Q_OS_WIN
    #include <QProcess>
#else
    #include <QDesktopServices>
    #include <QUrl>
#endif


bool isRichText(const QString &filename)
{
    return filename.endsWith(".gwe") || filename.endsWith(".htm") || filename.endsWith(".html");
}

const QString getFileName(const QString &fileName)
{
    return fileName.mid(fileName.lastIndexOf('/') + 1);
}

const QString getFileName(const QFile *file)
{
    return file ? getFileName(file->fileName()) : QObject::tr("New document");
}


// ///////////////////////////////////////////////////////////////////////////////////////////////////////// //


QAction* Editor::saveAsRichTextByDefault = nullptr;
const QString Editor::defaultStyleSheet = "a { color: #2A78CC; }";


Editor::Editor(QWidget *parent, QFont const& font, int tabSize)
    : QTextEdit(parent),
    file(nullptr), changed(false),
    lastSavedUndoSteps(0), lastPressIsUndoRedo(false),
    autosaveTimer(this)
{
    setFont(font);
    setCurrentFont(font);
    setTabStopDistance(tabSize);
    setTextBackgroundColor(Qt::white);

    QObject::connect(document(), SIGNAL(undoCommandAdded()), this, SLOT(undoCommandAdded()));
    QObject::connect(this, SIGNAL(textChanged()), this, SLOT(fileChanged()));
    QObject::connect(&autosaveTimer, SIGNAL(timeout()), this, SLOT(autosave()));

    QPalette p = palette();
    p.setColor(QPalette::Highlight, QColor(/*"#0094FF"*/"#0078D7"));
    p.setColor(QPalette::HighlightedText, Qt::white);
    setPalette(p);

    document()->setDefaultStyleSheet(defaultStyleSheet);
    setTextBackgroundColor(Qt::transparent);
}

Editor::~Editor()
{
    delete file;
}


bool Editor::showInExplorer()
{
    if(!file)
        return false;
#ifdef Q_OS_WIN
    return QProcess::startDetached("explorer", QStringList()<<"/select,"<<QDir::toNativeSeparators(file->fileName()));
#else
    return QDesktopServices::openUrl(QUrl("file:"+file->fileName()).adjusted(QUrl::RemoveFilename));
#endif
}


bool Editor::open(const QString &path)
{
    bool success = true;
    QFile *newFile = new QFile(path);

    if(newFile->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        delete file;
        file = newFile;

        QTextStream flux(file);

        if(isRichText(file->fileName()))
            setHtml(flux.readAll());
        else
            setPlainText(flux.readAll());

        file->close();

        changed = false;
        emit newName(getFileName(), index, QString());
    }
    else
    {
        delete newFile;
        success = false;
    }

    return success;
}


QString& cleanupHTML(QString html)
{
    // replace is safe because non-HTML '>' are translated to '&gt;'
    return html.replace(QRegularExpression(" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">"), ">")
        .replace(QRegularExpression("text/css\">"), "text/css\">p, pre{margin-top:0px;margin-bottom:0px;}")
        .replace(QRegularExpression("<span style=\" background-color:transparent;\">(.*)</span>"), "\\1")
        .replace(QRegularExpression("<pr?e?><span style=\" font-family:'Courier New';\">(.*)</span></pr?e?>"), "<pre>\\1</pre>")
        .replace(QRegularExpression("<pr?e? style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;( font-family:'([^'])*';)?\"><br /></pr?e?>"), "<p style=\"-qt-paragraph-type:empty;\"></p>");
}

bool Editor::save()
{
    bool res;

    if(!file)
        res = saveAs();
    else
    {
        res = file->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
        QString fileName = file->fileName();

        if(res)
        {
            QTextStream flux(file);
            if(isRichText(fileName))
                flux<<cleanupHTML(toHtml());
            else
                flux<<toPlainText();

            file->close();
            lastSavedUndoSteps = document()->availableUndoSteps();
            // To force the document to create a new undo step
            // 'tis a hack, yes; 'tis also not my fault this can't simply be done with a method
            // And this is needed because otherwise, when saving then writing on the same line, the lastSavedUndoStep will not actually be where the user saved
            insertPlainText("\n");
            undo();
            document()->clearUndoRedoStacks(QTextDocument::RedoStack);
            changed = false;

            emit newName(getFileName(), index, "*");

            autosaveTimer.start();
        }
        else
            QMessageBox::critical(this, tr("Saving error"), tr("Saving impossible at\n%1\nCheck that the device %2 has not been removed and that the file is not read-only.").arg(fileName).arg(fileName.left(2)));
    }

    return res;
}


bool Editor::saveAs()
{
    bool res = false;
    QString name(QFileDialog::getSaveFileName(this, tr("Save a file"), QString(),
        saveAsRichTextByDefault->isChecked()
            ? tr("GWE rich text (*.gwe);;Raw text (*.txt);;HTML rich text (*.htm; *.html);;Other file (*.*)")
            : tr("Raw text (*.txt);;GWE rich text (*.gwe);;HTML rich text (*.htm; *.html);;Other file (*.*)")));

    if(!name.isEmpty())
    {
        if(!file || name != file->fileName())
        {
            delete file;
            file = new QFile(name);
        }

        changed = true;
        res = save();
    }

    return res;
}


bool Editor::rename()
{
    int result;

    if(file)
    {
        const QString oldName(file->fileName());
        QString newFileName;
        bool fileAlreadyExists;

        do {
            RenameWindow renameWin(this, getFileName());
            result = renameWin.exec();
            if(result)
            {
                newFileName = renameWin.getName();

                if(newFileName == oldName)
                {
                    save();
                    return true;
                }

                fileAlreadyExists = QFile::exists(newFileName);

                if(fileAlreadyExists)
                    QMessageBox::critical(this, tr("Renaming impossible"), tr("\"%1\" already exists in the same directory.").arg(newFileName));
            }
            else
                fileAlreadyExists = false;

        }while(fileAlreadyExists);

        if(result)
        {
            bool unwantedDowngrade = isRichText(oldName) && !isRichText(newFileName);
            if(unwantedDowngrade)
            {
                auto confirm = QMessageBox::warning(this,
                    tr("Format warning"), tr("If you switch to *.txt format, you will lose all text formatting. Confirm?"),
                    QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::Cancel,
                    QMessageBox::StandardButton::Cancel);
                unwantedDowngrade = confirm != QMessageBox::StandardButton::Yes;
            }

            if(unwantedDowngrade)
            {
                result = false;
            }
            else
            {
                result = file->rename(newFileName);

                if(result)
                {
                    changed = true;
                    if(save())
                        emit newName(file->fileName(), index, oldName);
                    else
                        result = false;
                }
                else
                    QMessageBox::critical(this, tr("Renaming error"), tr("The file could not be renamed."));
            }
        }
    }
    else
        result = saveAs();

    return result;
}

void Editor::copyFullPath()
{
    if(file)
        QGuiApplication::clipboard()->setText(QDir::toNativeSeparators(file->fileName()));
}


void Editor::fileChanged()
{
    changed = !lastPressIsUndoRedo || document()->availableUndoSteps() != lastSavedUndoSteps;
    if(changed)
        emit newName("* " + ::getFileName(file), index, QString());
    else
        emit newName(getFileName(), index, QString());
}

void Editor::undoCommandAdded()
{
    if(document()->availableUndoSteps() == lastSavedUndoSteps) // the last saved undo step was overwritten
        lastSavedUndoSteps = -1;
}


void Editor::changeColor()
{
    QColor color = QColorDialog::getColor(textColor(), this, tr("Choose a color"));
    if(color.isValid())
        setTextColor(color);
}

void Editor::changeBackgroundColor()
{
    if(QMessageBox::question(this, "Background color", "Do you want to remove the background color ?") == QMessageBox::Yes)
    {
        setTextBackgroundColor(Qt::transparent);
        return;
    }

    QColor color = QColorDialog::getColor(textBackgroundColor(), this, tr("Choose a background color"));
    if(color.isValid())
        setTextBackgroundColor(color);
}

void Editor::upper()
{
    QTextCursor cursor(textCursor());

    if(cursor.hasSelection())
    {
        int anchor = cursor.anchor(),
            position = cursor.position();
        insertHtml(cursor.selection().toHtml().toUpper());
        cursor.setPosition(anchor);
        cursor.setPosition(position, QTextCursor::KeepAnchor);
        setTextCursor(cursor);
    }
}

void Editor::lower()
{
    QTextCursor cursor(textCursor());

    if(cursor.hasSelection())
    {
        int anchor = cursor.anchor(),
            position = cursor.position();
        insertHtml(cursor.selection().toHtml().toLower());
        cursor.setPosition(anchor);
        cursor.setPosition(position, QTextCursor::KeepAnchor);
        setTextCursor(cursor);
    }
}

void Editor::changeAutosaveDelay(int milliseconds)
{
    autosaveTimer.stop();
    if(milliseconds > 0)
    {
        autosaveTimer.setInterval(milliseconds);
        autosaveTimer.start();
    }
}

void Editor::autosave()
{
    if(file && changed)
        save();
}



void Editor::keyPressEvent(QKeyEvent *event)
{
    lastPressIsUndoRedo = true;

    if(event == QKeySequence::Undo)
        undo();
    else if(event == QKeySequence::Redo)
        redo();
    else
    {
        lastPressIsUndoRedo = false;
        QTextEdit::keyPressEvent(event);
    }
}


void Editor::dropEvent(QDropEvent *event)
{
    if(!event->mimeData()->hasUrls())
        QTextEdit::dropEvent(event);
    else
    {
        const QList<QUrl>& urls = event->mimeData()->urls();
        QStringList files;

        for(int i = 0 ; i < urls.size() ; i++)
        {
            const QUrl &url = urls[i];
            if(url.isLocalFile())
                files<<url.toString(QUrl::PreferLocalFile);
        }

        if(files.isEmpty())
            QTextEdit::dropEvent(event);
        else
            emit filesDropped(files);
    }
}
