// //////////////////////////////////////////////////////////
//
// GWE - GpaWa Editor
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Editor.
//
// GpaWa Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Editor. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "recentfileslist.hpp"


RecentFilesList::RecentFilesList() :
    max(0), pathsCount(0),
    menu(nullptr), a_openAll(nullptr), a_clear(nullptr),
    master(nullptr)
{}

RecentFilesList::RecentFilesList(int maxPaths, QMenu *actionsMenu, QWidget *master) :
    max(maxPaths), pathsCount(0),
    menu(actionsMenu), a_openAll(menu->actions()[0]), a_clear(menu->actions()[1]),
    master(master)
{
    QFile file(FILE_PATH);
    if(!file.exists())
    {
        file.open(QIODevice::WriteOnly);
        file.close();
    }

    if(file.open(QIODevice::ReadOnly))
    {
        QTextStream stream(&file);

        while(!stream.atEnd())
        {
            QString filePath(stream.readLine());
            dates.append(stream.readLine());

            if(!filePath.isEmpty())
            {
                filePath.replace('\\', '/');
                menu->addAction(getFileName(filePath))->setToolTip(filePath);
                pathsCount++;
            }
        }

        file.close();

        if(pathsCount == 0)
        {
            a_openAll->setEnabled(false);
            a_clear->setEnabled(false);
        }
        else
        {
            setMax(max); // to insure there is not more recent files than allowed
            updateMenu();
        }
    }
    else
    {
        QMessageBox::warning(master, QObject::tr("Error"), QObject::tr("The recent files list could not be loaded."));
    }
}

void RecentFilesList::setMax(int maximum)
{
    max = maximum;

    QList<QAction*> recentFilesActions(menu->actions());

    for(int i = recentFilesActions.size() - 1 ; pathsCount > max ; i--)
        remove(i);
}

QStringList RecentFilesList::getAllPaths()
{
    QStringList paths;

    for(int i = 0 ; i + OFFSET < menu->actions().size() ; i++)
        paths<<menu->actions()[i+OFFSET]->toolTip();

    return paths;
}

void RecentFilesList::add(QString path)
{
    path.replace('\\', '/');
    int pos = indexOf(path);
    if(pos == -1)
    {
        menu->addAction(getFileName(path))->setToolTip(path);
        dates.append(QDate::currentDate().toString(DATE_FORMAT));

        moveToStart(pathsCount);
        pathsCount++;

        if(pathsCount > max)
            remove(max);
    }
    else
        moveToStart(pos);

    updateMenu();
}

bool RecentFilesList::remove(int pos)
{
    if(pos < 0)
        return false;

    QAction *recentFileAction = menu->actions()[pos+OFFSET];
    menu->removeAction(recentFileAction);
    dates.removeAt(pos);
    delete recentFileAction;
    pathsCount--;

    updateMenu();
    return true;
}

bool RecentFilesList::remove(const QString& path)
{
    return remove(indexOf(path));
}

void RecentFilesList::moveToStart(int pos)
{
    if(pos > 0)
    {
        QList<QAction*> actions = menu->actions();

        menu->removeAction(actions[pos+OFFSET]);
        menu->insertAction(actions[OFFSET], actions[pos+OFFSET]);
        dates.removeAt(pos);
        dates.prepend(QDate::currentDate().toString(DATE_FORMAT));
    }
}

void RecentFilesList::moveToStart(const QString& path)
{
    moveToStart(indexOf(path));
}

void RecentFilesList::updateMenu()
{
    if(pathsCount == 0)
    {
        a_openAll->setEnabled(false);
        a_clear->setEnabled(false);
    }
    else
    {
        a_openAll->setEnabled(true);
        a_clear->setEnabled(true);
        QList<QAction*> actions(menu->actions());

        for(int i = OFFSET ; i < actions.size() && i - OFFSET < 10 ; i++)
            actions[i]->setShortcut(QKeySequence("CTRL+" + QString::number(i-OFFSET)));
    }
}

void RecentFilesList::clearList()
{
    a_openAll->setEnabled(false);
    a_clear->setEnabled(false);

    QList<QAction*> actions(menu->actions());

    for(int i = OFFSET ; i < actions.size() && i - OFFSET < 10 ; i++)
    {
        menu->removeAction(actions[i]);
        delete actions[i];
    }

    dates.clear();
    pathsCount = 0;
}

bool RecentFilesList::saveList()
{
    QFile file(FILE_PATH);
    const bool success = file.open(QIODevice::WriteOnly);

    if(success)
    {
        QTextStream stream(&file);

        if(pathsCount != 0)
            stream<<path(0)<<'\n'<<dates[0];

        for(int i = 1 ; i < pathsCount ; i++)
            stream<<'\n'<<path(i)<<'\n'<<dates[i];

        file.close();
    }
    else
    {
        QMessageBox::warning(master, QObject::tr("Error"), QObject::tr("The recent files list could not be saved."));
    }

    return success;
}
